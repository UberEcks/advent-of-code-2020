package com.uberx.aoc2020.day20;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import org.apache.commons.lang3.mutable.MutableLong;
import org.apache.commons.lang3.tuple.Pair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * --- Day 20: Jurassic Jigsaw ---
 * <p>
 * The high-speed train leaves the forest and quickly carries you south. You can even see a desert in the distance!
 * Since you have some spare time, you might as well see if there was anything interesting in the image the Mythical
 * Information Bureau satellite captured.
 * <p>
 * <br>
 * <p>
 * After decoding the satellite messages, you discover that the data actually contains many small images created by the
 * satellite's camera array. The camera array consists of many cameras; rather than produce a single square image, they
 * produce many smaller square image tiles that need to be reassembled back into a single image.
 * <p>
 * <br>
 * <p>
 * Each camera in the camera array returns a single monochrome image tile with a random unique ID number. The tiles
 * (your puzzle input) arrived in a random order.
 * <p>
 * <br>
 * <p>
 * Worse yet, the camera array appears to be malfunctioning: each image tile has been rotated and flipped to a random
 * orientation. Your first task is to reassemble the original image by orienting the tiles so they fit together.
 * <p>
 * <br>
 * <p>
 * To show how the tiles should be reassembled, each tile's image data includes a border that should line up exactly
 * with its adjacent tiles. All tiles have this border, and the border lines up exactly when the tiles are both oriented
 * correctly. Tiles at the edge of the image also have this border, but the outermost edges won't line up with any other
 * tiles.
 * <p>
 * <br>
 * <p>
 * For example, suppose you have the following nine tiles:
 * <p>
 * <br>
 * <p>
 * Tile 2311:
 * <p>
 * ..##.#..#.
 * <p>
 * ##..#.....
 * <p>
 * #...##..#.
 * <p>
 * ####.#...#
 * <p>
 * ##.##.###.
 * <p>
 * ##...#.###
 * <p>
 * .#.#.#..##
 * <p>
 * ..#....#..
 * <p>
 * ###...#.#.
 * <p>
 * ..###..###
 * <p>
 * <br>
 * <p>
 * Tile 1951:
 * <p>
 * #.##...##.
 * <p>
 * #.####...#
 * <p>
 * .....#..##
 * <p>
 * #...######
 * <p>
 * .##.#....#
 * <p>
 * .###.#####
 * <p>
 * ###.##.##.
 * <p>
 * .###....#.
 * <p>
 * ..#.#..#.#
 * <p>
 * #...##.#..
 * <p>
 * <br>
 * <p>
 * Tile 1171:
 * <p>
 * ####...##.
 * <p>
 * #..##.#..#
 * <p>
 * ##.#..#.#.
 * <p>
 * .###.####.
 * <p>
 * ..###.####
 * <p>
 * .##....##.
 * <p>
 * .#...####.
 * <p>
 * #.##.####.
 * <p>
 * ####..#...
 * <p>
 * .....##...
 * <p>
 * <br>
 * <p>
 * Tile 1427:
 * <p>
 * ###.##.#..
 * <p>
 * .#..#.##..
 * <p>
 * .#.##.#..#
 * <p>
 * #.#.#.##.#
 * <p>
 * ....#...##
 * <p>
 * ...##..##.
 * <p>
 * ...#.#####
 * <p>
 * .#.####.#.
 * <p>
 * ..#..###.#
 * <p>
 * ..##.#..#.
 * <p>
 * <br>
 * <p>
 * Tile 1489:
 * <p>
 * ##.#.#....
 * <p>
 * ..##...#..
 * <p>
 * .##..##...
 * <p>
 * ..#...#...
 * <p>
 * #####...#.
 * <p>
 * #..#.#.#.#
 * <p>
 * ...#.#.#..
 * <p>
 * ##.#...##.
 * <p>
 * ..##.##.##
 * <p>
 * ###.##.#..
 * <p>
 * <br>
 * <p>
 * Tile 2473:
 * <p>
 * #....####.
 * <p>
 * #..#.##...
 * <p>
 * #.##..#...
 * <p>
 * ######.#.#
 * <p>
 * .#...#.#.#
 * <p>
 * .#########
 * <p>
 * .###.#..#.
 * <p>
 * ########.#
 * <p>
 * ##...##.#.
 * <p>
 * ..###.#.#.
 * <p>
 * <br>
 * <p>
 * Tile 2971:
 * <p>
 * ..#.#....#
 * <p>
 * #...###...
 * <p>
 * #.#.###...
 * <p>
 * ##.##..#..
 * <p>
 * .#####..##
 * <p>
 * .#..####.#
 * <p>
 * #..#.#..#.
 * <p>
 * ..####.###
 * <p>
 * ..#.#.###.
 * <p>
 * ...#.#.#.#
 * <p>
 * <br>
 * <p>
 * Tile 2729:
 * <p>
 * ...#.#.#.#
 * <p>
 * ####.#....
 * <p>
 * ..#.#.....
 * <p>
 * ....#..#.#
 * <p>
 * .##..##.#.
 * <p>
 * .#.####...
 * <p>
 * ####.#.#..
 * <p>
 * ##.####...
 * <p>
 * ##..#.##..
 * <p>
 * #.##...##.
 * <p>
 * <br>
 * <p>
 * Tile 3079:
 * <p>
 * #.#.#####.
 * <p>
 * .#..######
 * <p>
 * ..#.......
 * <p>
 * ######....
 * <p>
 * ####.#..#.
 * <p>
 * .#...#.##.
 * <p>
 * #.#####.##
 * <p>
 * ..#.###...
 * <p>
 * ..#.......
 * <p>
 * ..#.###...
 * <p>
 * <br>
 * <p>
 * By rotating, flipping, and rearranging them, you can find a square arrangement that causes all adjacent borders to
 * line up:
 * <p>
 * <br>
 * <p>
 * #...##.#.. ..###..### #.#.#####.
 * <p>
 * ..#.#..#.# ###...#.#. .#..######
 * <p>
 * .###....#. ..#....#.. ..#.......
 * <p>
 * ###.##.##. .#.#.#..## ######....
 * <p>
 * .###.##### ##...#.### ####.#..#.
 * <p>
 * .##.#....# ##.##.###. .#...#.##.
 * <p>
 * #...###### ####.#...# #.#####.##
 * <p>
 * .....#..## #...##..#. ..#.###...
 * <p>
 * #.####...# ##..#..... ..#.......
 * <p>
 * #.##...##. ..##.#..#. ..#.###...
 * <p>
 * <br>
 * <p>
 * #.##...##. ..##.#..#. ..#.###...
 * <p>
 * ##..#.##.. ..#..###.# ##.##....#
 * <p>
 * ##.####... .#.####.#. ..#.###..#
 * <p>
 * ####.#.#.. ...#.##### ###.#..###
 * <p>
 * .#.####... ...##..##. .######.##
 * <p>
 * .##..##.#. ....#...## #.#.#.#...
 * <p>
 * ....#..#.# #.#.#.##.# #.###.###.
 * <p>
 * ..#.#..... .#.##.#..# #.###.##..
 * <p>
 * ####.#.... .#..#.##.. .######...
 * <p>
 * ...#.#.#.# ###.##.#.. .##...####
 * <p>
 * <br>
 * <p>
 * ...#.#.#.# ###.##.#.. .##...####
 * <p>
 * ..#.#.###. ..##.##.## #..#.##..#
 * <p>
 * ..####.### ##.#...##. .#.#..#.##
 * <p>
 * #..#.#..#. ...#.#.#.. .####.###.
 * <p>
 * .#..####.# #..#.#.#.# ####.###..
 * <p>
 * .#####..## #####...#. .##....##.
 * <p>
 * ##.##..#.. ..#...#... .####...#.
 * <p>
 * #.#.###... .##..##... .####.##.#
 * <p>
 * #...###... ..##...#.. ...#..####
 * <p>
 * ..#.#....# ##.#.#.... ...##.....
 * <p>
 * <br>
 * <p>
 * For reference, the IDs of the above tiles are:
 * <p>
 * <br>
 * <p>
 * 1951    2311    3079
 * <p>
 * 2729    1427    2473
 * <p>
 * 2971    1489    1171
 * <p>
 * <br>
 * <p>
 * To check that you've assembled the image correctly, multiply the IDs of the four corner tiles together. If you do
 * this with the assembled tiles from the example above, you get 1951 * 3079 * 2971 * 1171 = 20899048083289.
 * <p>
 * <br>
 * <p>
 * Assemble the tiles into an image. What do you get if you multiply together the IDs of the four corner tiles?
 * <p>
 * <br>
 * <p>
 * --- Part Two ---
 * <p>
 * Now, you're ready to check the image for sea monsters.
 * <p>
 * <br>
 * <p>
 * The borders of each tile are not part of the actual image; start by removing them.
 * <p>
 * <br>
 * <p>
 * In the example above, the tiles become:
 * <p>
 * <br>
 * <p>
 * .#.#..#. ##...#.# #..#####
 * <p>
 * ###....# .#....#. .#......
 * <p>
 * ##.##.## #.#.#..# #####...
 * <p>
 * ###.#### #...#.## ###.#..#
 * <p>
 * ##.#.... #.##.### #...#.##
 * <p>
 * ...##### ###.#... .#####.#
 * <p>
 * ....#..# ...##..# .#.###..
 * <p>
 * .####... #..#.... .#......
 * <p>
 * <br>
 * <p>
 * #..#.##. .#..###. #.##....
 * <p>
 * #.####.. #.####.# .#.###..
 * <p>
 * ###.#.#. ..#.#### ##.#..##
 * <p>
 * #.####.. ..##..## ######.#
 * <p>
 * ##..##.# ...#...# .#.#.#..
 * <p>
 * ...#..#. .#.#.##. .###.###
 * <p>
 * .#.#.... #.##.#.. .###.##.
 * <p>
 * ###.#... #..#.##. ######..
 * <p>
 * <br>
 * <p>
 * .#.#.### .##.##.# ..#.##..
 * <p>
 * .####.## #.#...## #.#..#.#
 * <p>
 * ..#.#..# ..#.#.#. ####.###
 * <p>
 * #..####. ..#.#.#. ###.###.
 * <p>
 * #####..# ####...# ##....##
 * <p>
 * #.##..#. .#...#.. ####...#
 * <p>
 * .#.###.. ##..##.. ####.##.
 * <p>
 * ...###.. .##...#. ..#..###
 * <p>
 * <br>
 * <p>
 * Remove the gaps to form the actual image:
 * <p>
 * <br>
 * <p>
 * .#.#..#.##...#.##..#####
 * <p>
 * ###....#.#....#..#......
 * <p>
 * ##.##.###.#.#..######...
 * <p>
 * ###.#####...#.#####.#..#
 * <p>
 * ##.#....#.##.####...#.##
 * <p>
 * ...########.#....#####.#
 * <p>
 * ....#..#...##..#.#.###..
 * <p>
 * .####...#..#.....#......
 * <p>
 * #..#.##..#..###.#.##....
 * <p>
 * #.####..#.####.#.#.###..
 * <p>
 * ###.#.#...#.######.#..##
 * <p>
 * #.####....##..########.#
 * <p>
 * ##..##.#...#...#.#.#.#..
 * <p>
 * ...#..#..#.#.##..###.###
 * <p>
 * .#.#....#.##.#...###.##.
 * <p>
 * ###.#...#..#.##.######..
 * <p>
 * .#.#.###.##.##.#..#.##..
 * <p>
 * .####.###.#...###.#..#.#
 * <p>
 * ..#.#..#..#.#.#.####.###
 * <p>
 * #..####...#.#.#.###.###.
 * <p>
 * #####..#####...###....##
 * <p>
 * #.##..#..#...#..####...#
 * <p>
 * .#.###..##..##..####.##.
 * <p>
 * ...###...##...#...#..###
 * <p>
 * <br>
 * <p>
 * Now, you're ready to search for sea monsters! Because your image is monochrome, a sea monster will look like this:
 * <p>
 * <br>
 * <p>
 * #
 * <p>
 * #    ##    ##    ###
 * <p>
 * #  #  #  #  #  #
 * <p>
 * <br>
 * <p>
 * When looking for this pattern in the image, the spaces can be anything; only the # need to match. Also, you might
 * need to rotate or flip your image before it's oriented correctly to find sea monsters. In the above image, after
 * flipping and rotating it to the appropriate orientation, there are two sea monsters (marked with O):
 * <p>
 * <br>
 * <p>
 * .####...#####..#...###..
 * <p>
 * #####..#..#.#.####..#.#.
 * <p>
 * .#.#...#.###...#.##.O#..
 * <p>
 * #.O.##.OO#.#.OO.##.OOO##
 * <p>
 * ..#O.#O#.O##O..O.#O##.##
 * <p>
 * ...#.#..##.##...#..#..##
 * <p>
 * #.##.#..#.#..#..##.#.#..
 * <p>
 * .###.##.....#...###.#...
 * <p>
 * #.####.#.#....##.#..#.#.
 * <p>
 * ##...#..#....#..#...####
 * <p>
 * ..#.##...###..#.#####..#
 * <p>
 * ....#.##.#.#####....#...
 * <p>
 * ..##.##.###.....#.##..#.
 * <p>
 * #...#...###..####....##.
 * <p>
 * .#.##...#.##.#.#.###...#
 * <p>
 * #.###.#..####...##..#...
 * <p>
 * #.###...#.##...#.##O###.
 * <p>
 * .O##.#OO.###OO##..OOO##.
 * <p>
 * ..O#.O..O..O.#O##O##.###
 * <p>
 * #.#..##.########..#..##.
 * <p>
 * #.#####..#.#...##..#....
 * <p>
 * #....##..#.#########..##
 * <p>
 * #...#.....#..##...###.##
 * <p>
 * #..###....##.#...##.##.#
 * <p>
 * <br>
 * <p>
 * Determine how rough the waters are in the sea monsters' habitat by counting the number of # that are not part of a
 * sea monster. In the above example, the habitat's water roughness is 273.
 * <p>
 * <br>
 * <p>
 * How many # are not part of a sea monster?
 */
public class Day20 {
    public static List<Tile> getTiles(final String filename) throws IOException {
        final List<Tile> tiles = new ArrayList<>();
        try (final BufferedReader reader = new BufferedReader(
                new InputStreamReader(Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename))))) {
            String line;
            while ((line = reader.readLine()) != null) {
                final int id = Integer.parseInt(line.substring(5, 9));
                final Character[][] grid = new Character[10][10];
                for (int i = 0; i < 10; i++) {
                    grid[i] = Arrays.stream(reader.readLine().split(""))
                            .map(token -> token.charAt(0))
                            .toArray(Character[]::new);
                }
                tiles.add(new Tile(id, grid));
                reader.readLine();
            }
        }
        return tiles;
    }

    public long productOfCornerTileIds(final List<Tile> tiles) {
        final List<Tile> corners = getCorners(tiles);
        final MutableLong product = new MutableLong(1);
        corners.forEach(tile -> product.setValue(product.longValue() * tile.getId()));
        return product.getValue();
    }

    public int waterRoughnessOfFinalImageWithSeaMonsters(final List<Tile> tiles,
                                                         final int rows,
                                                         final int cols) {
        final List<Tile> corners = getCorners(tiles);
        final Pair<Tile, Integer> imageWithSeaMonsters = findSeaMonsters(tiles, corners, rows, cols);
        int waterRoughness = 0;
        final Character[][] grid = imageWithSeaMonsters.getKey().getGrid();
        for (final Character[] row : grid) {
            for (int col = 0; col < grid[0].length; col++) {
                if (row[col] == '#') {
                    waterRoughness++;
                }
            }
        }
        return waterRoughness - 15 * imageWithSeaMonsters.getRight();
    }

    private List<Tile> getCorners(final List<Tile> tiles) {
        final Map<Tile, Map<Tile.Edge.State, Pair<Tile, Tile.Edge.State>>> allConnections = computeConnections(tiles);
        return tiles.stream()
                .filter(tile -> allConnections.get(tile).size() == 4)
                .collect(Collectors.toList());
    }

    private Pair<Tile, Integer> findSeaMonsters(final List<Tile> tiles,
                                                final List<Tile> corners,
                                                final int rows,
                                                final int cols) {
        final Tile[][] image = getImage(tiles, corners, rows, cols);
        final Tile[][] trimmedImage = Arrays.stream(image)
                .map(row -> Arrays.stream(row)
                        .map(Tile::trimEdges)
                        .toArray(Tile[]::new))
                .toArray(Tile[][]::new);

        final Tile mergedImage = mergeTiles(trimmedImage);

        // Search in current state
        Pair<Tile, Integer> result = searchSeaMonsters(mergedImage);
        if (result.getRight() > 0) {
            return result;
        }

        // Search in flipped state
        mergedImage.flipAround(Tile.Edge.State.TOP_REVERSED);
        result = searchSeaMonsters(mergedImage);
        mergedImage.flipAround(Tile.Edge.State.TOP_REVERSED);
        return result;
    }

    private Tile[][] getImage(final List<Tile> tiles,
                              final List<Tile> corners,
                              final int rows,
                              final int cols) {
        final Map<Tile, Map<Tile.Edge.State, Pair<Tile, Tile.Edge.State>>> allConnections = computeConnections(tiles);
        final Tile topLeftTile = corners.stream()
                .filter(corner -> allConnections.get(corner)
                        .keySet()
                        .equals(Sets.newHashSet(
                                Tile.Edge.State.RIGHT,
                                Tile.Edge.State.RIGHT_REVERSED,
                                Tile.Edge.State.BOTTOM,
                                Tile.Edge.State.BOTTOM_REVERSED)))
                .findFirst()
                .get();

        final Tile[][] image = new Tile[rows][cols];
        image[0][0] = topLeftTile;
        for (int col = 1; col < cols; col++) {
            final Pair<Tile, Tile.Edge.State> rightTileAndState = allConnections.get(image[0][col - 1])
                    .get(Tile.Edge.State.RIGHT);
            rightTileAndState.getLeft().reorient(rightTileAndState.getRight(), Tile.Edge.State.RIGHT);
            image[0][col] = rightTileAndState.getLeft();
            allConnections.putAll(computeConnections(tiles));
        }

        for (int col = 0; col < cols; col++) {
            for (int row = 1; row < rows; row++) {
                final Pair<Tile, Tile.Edge.State> bottomTileAndState = allConnections.get(image[row - 1][col])
                        .get(Tile.Edge.State.BOTTOM);
                bottomTileAndState.getLeft().reorient(bottomTileAndState.getRight(), Tile.Edge.State.BOTTOM);
                image[row][col] = bottomTileAndState.getLeft();
                allConnections.putAll(computeConnections(tiles));
            }
        }
        return image;
    }

    private Tile mergeTiles(final Tile[][] trimmedImage) {
        final Character[][] grid = new Character
                [trimmedImage.length * trimmedImage[0][0].getGrid().length]
                [trimmedImage[0].length * trimmedImage[0][0].getGrid()[0].length];
        for (int tileRow = 0; tileRow < trimmedImage.length; tileRow++) {
            for (int row = 0; row < trimmedImage[0][0].getGrid().length; row++) {
                for (int tileCol = 0; tileCol < trimmedImage[0].length; tileCol++) {
                    for (int col = 0; col < trimmedImage[0][0].getGrid()[0].length; col++) {
                        final Tile currentTile = trimmedImage[tileRow][tileCol];
                        grid
                                [tileRow * trimmedImage[0][0].getGrid().length + row]
                                [tileCol * trimmedImage[0][0].getGrid()[0].length + col]
                                = currentTile.getGrid()[row][col];
                    }

                }
            }
        }
        return new Tile(0, grid);
    }

    private Pair<Tile, Integer> searchSeaMonsters(final Tile mergedImage) {
        int seaMonstersFound = numSeaMonstersFound(mergedImage);
        int rotations = 0;
        while (seaMonstersFound < 1 && rotations < 3) {
            mergedImage.rotateAnticlockwise();
            rotations++;
            seaMonstersFound = numSeaMonstersFound(mergedImage);
        }
        if (seaMonstersFound > 0) {
            return Pair.of(mergedImage, seaMonstersFound);
        }
        while (rotations < 4) {
            mergedImage.rotateAnticlockwise();
            rotations++;
        }
        return Pair.of(mergedImage, 0);
    }

    private int numSeaMonstersFound(final Tile tile) {
        final Character[][] seaMonster = {
                {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', ' '},
                {'#', ' ', ' ', ' ', ' ', '#', '#', ' ', ' ', ' ', ' ', '#', '#', ' ', ' ', ' ', ' ', '#', '#', '#'},
                {' ', '#', ' ', ' ', '#', ' ', ' ', '#', ' ', ' ', '#', ' ', ' ', '#', ' ', ' ', '#', ' ', ' ', ' '}
        };
        final Character[][] grid = tile.getGrid();
        int seaMonsters = 0;
        for (int gridRow = 0; gridRow < grid.length - seaMonster.length; gridRow++) {
            for (int gridCol = 0; gridCol < grid[0].length - seaMonster[0].length; gridCol++) {
                boolean seaMonsterPossible = true;
                for (int smRow = 0; smRow < seaMonster.length && seaMonsterPossible; smRow++) {
                    for (int smCol = 0; smCol < seaMonster[0].length; smCol++) {
                        if (seaMonster[smRow][smCol] == '#' && grid[smRow + gridRow][smCol + gridCol] != '#') {
                            seaMonsterPossible = false;
                            break;
                        }
                    }
                }
                if (seaMonsterPossible) seaMonsters++;
            }
        }

        return seaMonsters;
    }

    private Map<Tile, Map<Tile.Edge.State, Pair<Tile, Tile.Edge.State>>> computeConnections(
            final List<Tile> tiles) {
        final Map<Tile, Map<Tile.Edge.State, Pair<Tile, Tile.Edge.State>>> allConnections = new HashMap<>();
        for (int i = 0; i < tiles.size(); i++) {
            final Tile currentTile = tiles.get(i);
            final Map<String, Tile.Edge.State> currentTileEdges = currentTile.computeEdges()
                    .stream()
                    .collect(Collectors.toMap(Tile.Edge::getRepresentation, Tile.Edge::getState));
            for (int j = 0; j < tiles.size(); j++) {
                if (j == i) continue;
                final Tile comparingTile = tiles.get(j);
                final Map<String, Tile.Edge.State> comparingTileEdges = comparingTile.computeEdges()
                        .stream()
                        .collect(Collectors.toMap(Tile.Edge::getRepresentation, Tile.Edge::getState));
                final Set<String> connections = Sets.intersection(
                        currentTileEdges.keySet(), comparingTileEdges.keySet());
                if (!connections.isEmpty()) {
                    if (!allConnections.containsKey(currentTile)) {
                        allConnections.put(currentTile, new LinkedHashMap<>());
                    }
                    allConnections.get(currentTile)
                            .putAll(connections
                                    .stream()
                                    .collect(Collectors.toMap(
                                            currentTileEdges::get,
                                            connection -> Pair.of(comparingTile, comparingTileEdges.get(connection)))));
                }
            }
        }
        return allConnections;
    }

    @Getter
    @ToString(of = {"id"})
    static class Tile {
        private static final List<Edge.State> ROTATIONS1 = Lists.newArrayList(
                Edge.State.TOP, Edge.State.LEFT_REVERSED, Edge.State.BOTTOM_REVERSED, Edge.State.RIGHT);
        private static final List<Edge.State> ROTATIONS2 = Lists.newArrayList(
                Edge.State.TOP_REVERSED, Edge.State.LEFT, Edge.State.BOTTOM, Edge.State.RIGHT_REVERSED);
        private static final Map<Edge.State, Edge.State> OPPOSITES = ImmutableMap.of(
                Edge.State.RIGHT, Edge.State.LEFT,
                Edge.State.BOTTOM, Edge.State.TOP);
        private final int id;
        private Character[][] grid;

        public Tile(int id, Character[][] grid) {
            this.id = id;
            this.grid = grid;
        }

        void reorient(final Edge.State original,
                      final Edge.State sourceConnection) {
            final Edge.State dest = OPPOSITES.get(sourceConnection);
            final boolean flip = rotate(original, dest);
            if (flip) {
                if (dest == Edge.State.LEFT) {
                    flipAround(Edge.State.LEFT_REVERSED);
                } else if (dest == Edge.State.TOP) {
                    flipAround(Edge.State.TOP_REVERSED);
                }
            }
        }

        private void flipAround(final Edge.State state) {
            if (state == Edge.State.TOP_REVERSED || state == Edge.State.BOTTOM_REVERSED) {
                for (int col = 0; col < grid[0].length / 2; col++) {
                    for (int row = 0; row < grid.length; row++) {
                        final char tmp = grid[row][col];
                        grid[row][col] = grid[row][grid[0].length - 1 - col];
                        grid[row][grid[0].length - 1 - col] = tmp;
                    }
                }
            } else if (state == Edge.State.LEFT_REVERSED || state == Edge.State.RIGHT_REVERSED) {
                for (int row = 0; row < grid.length / 2; row++) {
                    for (int col = 0; col < grid[0].length; col++) {
                        final char tmp = grid[row][col];
                        grid[row][col] = grid[grid.length - 1 - row][col];
                        grid[grid.length - 1 - row][col] = tmp;
                    }
                }
            }
        }

        private boolean rotate(final Edge.State src,
                               final Edge.State dest) {
            int rotations;
            boolean flip = false;
            if (ROTATIONS1.contains(src) && ROTATIONS1.contains(dest)) {
                rotations = ROTATIONS1.indexOf(dest) - ROTATIONS1.indexOf(src);
            } else if (ROTATIONS2.contains(src) && ROTATIONS2.contains(dest)) {
                rotations = ROTATIONS2.indexOf(dest) - ROTATIONS2.indexOf(src);
            } else if (ROTATIONS1.contains(src) && ROTATIONS2.contains(dest)) {
                rotations = ROTATIONS2.indexOf(dest) - ROTATIONS1.indexOf(src);
                flip = true;
            } else {
                rotations = ROTATIONS1.indexOf(dest) - ROTATIONS2.indexOf(src);
                flip = true;
            }
            final int antiClockwiseRotations = rotations < 0 ? rotations + 4 : rotations;
            for (int i = 0; i < antiClockwiseRotations; i++) {
                rotateAnticlockwise();
            }
            return flip;
        }

        private void rotateAnticlockwise() {
            for (int i = 0; i < grid.length / 2; i++) {
                for (int j = i; j < grid.length - i - 1; j++) {
                    final char temp = grid[i][j];

                    // Move values from right to top
                    grid[i][j] = grid[j][grid.length - 1 - i];

                    // Move values from bottom to right
                    grid[j][grid.length - 1 - i]
                            = grid[grid.length - 1 - i][grid.length - 1 - j];

                    // Move values from left to bottom
                    grid[grid.length - 1 - i][grid.length - 1 - j] = grid[grid.length - 1 - j][i];

                    // Assign temp to left
                    grid[grid.length - 1 - j][i] = temp;
                }
            }
        }

        List<Edge> computeEdges() {
            final List<Edge> edges = new ArrayList<>();
            // top edge
            final StringBuilder topEdge = new StringBuilder();
            for (int i = 0; i < grid[0].length; i++) {
                topEdge.append(grid[0][i]);
            }
            edges.add(new Edge(topEdge.toString(), Edge.State.TOP));
            edges.add(new Edge(topEdge.reverse().toString(), Edge.State.TOP_REVERSED));
            // bottom edge
            final StringBuilder bottomEdge = new StringBuilder();
            for (int i = 0; i < grid[0].length; i++) {
                bottomEdge.append(grid[grid.length - 1][i]);
            }
            edges.add(new Edge(bottomEdge.toString(), Edge.State.BOTTOM));
            edges.add(new Edge(bottomEdge.reverse().toString(), Edge.State.BOTTOM_REVERSED));
            // left edge
            final StringBuilder leftEdge = new StringBuilder();
            for (final Character[] characters : grid) {
                leftEdge.append(characters[0]);
            }
            edges.add(new Edge(leftEdge.toString(), Edge.State.LEFT));
            edges.add(new Edge(leftEdge.reverse().toString(), Edge.State.LEFT_REVERSED));
            // right edge
            final StringBuilder rightEdge = new StringBuilder();
            for (final Character[] characters : grid) {
                rightEdge.append(characters[grid[0].length - 1]);
            }
            edges.add(new Edge(rightEdge.toString(), Edge.State.RIGHT));
            edges.add(new Edge(rightEdge.reverse().toString(), Edge.State.RIGHT_REVERSED));

            return edges;
        }

        Tile trimEdges() {
            final Character[][] trimmedGrid = new Character[grid.length - 2][grid[0].length - 2];
            for (int row = 1; row < grid.length - 1; row++) {
                for (int col = 1; col < grid[0].length - 1; col++) {
                    trimmedGrid[row - 1][col - 1] = grid[row][col];
                }
            }
            grid = trimmedGrid;
            return this;
        }

        @Getter
        @AllArgsConstructor
        static class Edge {
            private final String representation;
            private final State state;

            enum State {
                TOP, TOP_REVERSED,
                RIGHT, RIGHT_REVERSED,
                BOTTOM, BOTTOM_REVERSED,
                LEFT, LEFT_REVERSED
            }
        }
    }
}
