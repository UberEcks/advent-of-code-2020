package com.uberx.aoc2020.day14;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Day14 {
    private static final Pattern MEM_ASSIGNMENT_PATTERN = Pattern.compile(
            "mem\\[(?<destination>\\d+)] = (?<value>\\d+)");

    public static List<Instruction> getInstructions(final String filename) throws IOException {
        final List<Instruction> instructions = new ArrayList<>();
        try (final BufferedReader reader = new BufferedReader(
                new InputStreamReader(Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename))))) {
            Instruction instruction = null;
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.startsWith("mask = ")) {
                    instruction = new Instruction(line.substring(7));
                    instructions.add(instruction);
                } else if (instruction != null) {
                    instruction.addMemoryAssignment(line);
                }
            }
        }
        return instructions;
    }

    public long sumOfMemoryValues1(final List<Instruction> instructions) {
        final Map<Long, Long> memory = instructions.stream()
                .map(Instruction::execute1)
                .flatMap(subMemory -> subMemory.entrySet().stream())
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (v1, v2) -> v2,
                        LinkedHashMap::new));
        return memory.values()
                .stream()
                .mapToLong(Long::longValue)
                .sum();
    }

    public long sumOfMemoryValues2(final List<Instruction> instructions) {
        final Map<String, Long> memoryWithFloatingValues = instructions.stream()
                .map(Instruction::execute2)
                .flatMap(subMemory -> subMemory.entrySet().stream())
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (v1, v2) -> v2,
                        LinkedHashMap::new));
        final Map<Long, Long> memory = new LinkedHashMap<>();
        for (final Map.Entry<String, Long> entry : memoryWithFloatingValues.entrySet()) {
            final List<Long> floatingValues = getFloatingValues(entry.getKey());
            for (final Long floatingValue : floatingValues) {
                memory.put(floatingValue, entry.getValue());
            }
        }
        return memory.values()
                .stream()
                .mapToLong(Long::longValue)
                .sum();
    }

    private List<Long> getFloatingValues(final String maskedBinaryValue) {
        final int numFloatingBits = maskedBinaryValue.chars()
                .filter(ch -> ch == 'X')
                .map(i -> 1)
                .sum();
        final List<Long> floatingValues = new ArrayList<>();
        for (int i = 0; i < Math.pow(2, numFloatingBits); i++) {
            final String binary = "000000000000000000000000000000000000" + Integer.toBinaryString(i);
            final String paddedBinary = binary.substring(binary.length() - numFloatingBits);
            final StringBuilder binaryFloatingValue = new StringBuilder();
            int paddedBinaryIndex = 0;
            for (int j = 0; j < maskedBinaryValue.length(); j++) {
                if (maskedBinaryValue.charAt(j) == 'X') {
                    binaryFloatingValue.append(paddedBinary.charAt(paddedBinaryIndex));
                    paddedBinaryIndex++;
                } else {
                    binaryFloatingValue.append(maskedBinaryValue.charAt(j));
                }
            }
            floatingValues.add(Long.parseLong(binaryFloatingValue.toString(), 2));
        }
        return floatingValues;
    }

    @Getter
    @ToString
    static class Instruction {
        final String mask;
        final List<MemoryAssignment> memoryAssignments = new ArrayList<>();

        Instruction(String mask) {
            this.mask = mask;
        }

        void addMemoryAssignment(final String input) {
            memoryAssignments.add(MemoryAssignment.fromInput(input));
        }

        Map<Long, Long> execute1() {
            return memoryAssignments.stream()
                    .collect(Collectors.toMap(
                            MemoryAssignment::getDestination,
                            memoryAssignment ->
                                    Long.parseLong(
                                            applyMask1(Long.toBinaryString(memoryAssignment.getValue())),
                                            2),
                            (v1, v2) -> v2,
                            LinkedHashMap::new));
        }

        Map<String, Long> execute2() {
            return memoryAssignments.stream()
                    .collect(Collectors.toMap(
                            memoryAssignment -> applyMask2(Long.toBinaryString(memoryAssignment.getDestination())),
                            MemoryAssignment::getValue,
                            (v1, v2) -> v2,
                            LinkedHashMap::new));
        }

        String applyMask1(final String binary) {
            final StringBuilder result = new StringBuilder();
            for (int i = 0; i < mask.length(); i++) {
                if (i < mask.length() - binary.length()) {
                    result.append(mask.charAt(i) == 'X' ? '0' : mask.charAt(i));
                } else {
                    result.append(
                            mask.charAt(i) == 'X' ?
                                    binary.charAt(i - (mask.length() - binary.length())) :
                                    mask.charAt(i));
                }
            }
            return result.toString();
        }

        String applyMask2(final String binary) {
            final StringBuilder result = new StringBuilder();
            for (int i = 0; i < mask.length(); i++) {
                if (i < mask.length() - binary.length()) {
                    result.append(mask.charAt(i));
                } else {
                    result.append(
                            mask.charAt(i) == '0' ?
                                    binary.charAt(i - (mask.length() - binary.length())) :
                                    mask.charAt(i));
                }
            }
            return result.toString();
        }

        @Getter
        @AllArgsConstructor
        static class MemoryAssignment {
            private final long destination;
            private final long value;

            static MemoryAssignment fromInput(final String input) {
                final Matcher matcher = MEM_ASSIGNMENT_PATTERN.matcher(input);
                if (!matcher.find()) {
                    throw new RuntimeException("Unexpected input: " + input);
                }
                return new MemoryAssignment(
                        Long.parseLong(matcher.group("destination")),
                        Long.parseLong(matcher.group("value")));
            }

            public String toString() {
                return String.format("mem[%d] = %d", destination, value);
            }
        }
    }
}
