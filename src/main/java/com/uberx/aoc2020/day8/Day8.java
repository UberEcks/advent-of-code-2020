package com.uberx.aoc2020.day8;

import lombok.Getter;
import lombok.ToString;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.mutable.MutableInt;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Day8 {
    public static List<Instruction> getInstructions(final String filename) throws IOException {
        return IOUtils
                .readLines(
                        Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                        StandardCharsets.UTF_8)
                .stream()
                .map(line -> {
                    final String[] spaceDelimitedValues = line.split(" ");
                    final String operation = spaceDelimitedValues[0];
                    final int operand = Integer.parseInt(
                            spaceDelimitedValues[1].startsWith("+") ?
                                    spaceDelimitedValues[1].substring(1) : spaceDelimitedValues[1]);
                    return new Instruction(operation, operand);
                })
                .collect(Collectors.toList());
    }

    private void executeInstructions(final List<Instruction> instructions,
                                     final MutableInt accumulator) {
        for (int i = 0; i < instructions.size(); i++) {
            final Instruction currentInstruction = instructions.get(i);
            if (currentInstruction.getExecutionCount() > 0) {
                throw new IllegalStateException("Loop detected");
            }
            if ("acc".equals(currentInstruction.getOperation())) {
                accumulator.add(currentInstruction.getOperand());
            } else if ("jmp".equals(currentInstruction.getOperation())) {
                i += (currentInstruction.getOperand() - 1);
            }
            currentInstruction.incrementExecutionCount();
        }
    }

    public int accumulatorBeforeLoop(final List<Instruction> instructions) {
        final MutableInt accumulator = new MutableInt(0);
        try {
            executeInstructions(instructions, accumulator);
        } catch (final IllegalStateException e) {
            // ignore
        }
        return accumulator.getValue();
    }

    public int accumulatorAfterFix(final List<Instruction> instructions) {
        for (int i = 0; i < instructions.size(); i++) {
            if (!"jmp".equals(instructions.get(i).getOperation())
                    && !"nop".equals(instructions.get(i).getOperation())) {
                continue;
            }
            final List<Instruction> currentInstructions = copy(instructions);
            currentInstructions.get(i).flipOperation();
            final MutableInt accumulator = new MutableInt(0);
            try {
                executeInstructions(currentInstructions, accumulator);
            } catch (final IllegalStateException e) {
                continue;
            }
            return accumulator.getValue();
        }
        throw new RuntimeException("No fix found for instructions");
    }

    private List<Instruction> copy(final List<Instruction> instructions) {
        return instructions.stream()
                .map(instruction -> new Instruction(instruction.getOperation(), instruction.getOperand()))
                .collect(Collectors.toList());
    }

    @Getter
    @ToString
    static class Instruction {
        private String operation;
        private final int operand;
        private int executionCount = 0;

        public Instruction(String operation, int operand) {
            this.operation = operation;
            this.operand = operand;
        }

        public void incrementExecutionCount() {
            executionCount++;
        }

        public void flipOperation() {
            if ("jmp".equals(operation)) {
                operation = "nop";
            } else if ("nop".equals(operation)) {
                operation = "jmp";
            }
        }
    }
}
