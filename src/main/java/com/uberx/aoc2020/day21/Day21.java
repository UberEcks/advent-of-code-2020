package com.uberx.aoc2020.day21;

import com.google.common.collect.Sets;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class Day21 {
    public static List<Food> getFoods(final String filename) throws IOException {
        return IOUtils
                .readLines(
                        Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                        StandardCharsets.UTF_8)
                .stream()
                .map(line -> {
                    final int leftParenthesisIndex = line.indexOf('(');
                    final Set<String> ingredients = Arrays.stream(
                            line.substring(0, leftParenthesisIndex)
                                    .trim()
                                    .split(" "))
                            .collect(Collectors.toSet());
                    final Set<String> allergens = Arrays.stream(
                            line.substring(leftParenthesisIndex + 10, line.length() - 1)
                                    .trim()
                                    .split(","))
                            .map(String::trim)
                            .collect(Collectors.toSet());
                    return new Food(ingredients, allergens);
                })
                .collect(Collectors.toList());
    }

    public long numOccurrencesOfIngredientsWithoutAllergens(final List<Food> foods) {
        final Map<String, String> ingredientsToAllergens = getIngredientsAndAllergensMappings(foods);
        return foods.stream()
                .map(Food::getIngredients)
                .map(ingredients -> ingredients.stream()
                        .filter(ingredient -> !ingredientsToAllergens.containsKey(ingredient))
                        .collect(Collectors.toList()))
                .mapToLong(List::size)
                .sum();
    }

    public String canonicalDangerousIngredientList(final List<Food> foods) {
        final Map<String, String> ingredientsToAllergens = getIngredientsAndAllergensMappings(foods);
        return ingredientsToAllergens.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue())
                .map(Map.Entry::getKey)
                .collect(Collectors.joining(","));
    }

    private Map<String, String> getIngredientsAndAllergensMappings(final List<Food> foods) {
        final Map<String, String> ingredientsToAllergens = new HashMap<>();
        final Map<String, String> allergensToIngredients = new HashMap<>();
        final Map<String, Set<String>> allergensToPossibleIngredients = new HashMap<>();
        boolean done = false;
        while (!done) {
            int changes = 0;
            for (int i = 0; i < foods.size() - 1; i++) {
                for (int j = i + 1; j < foods.size(); j++) {
                    final Set<String> ingredients1 = foods.get(i).getIngredients()
                            .stream()
                            .filter(ingredient -> !ingredientsToAllergens.containsKey(ingredient))
                            .collect(Collectors.toSet());
                    final Set<String> allergens1 = foods.get(i).getAllergens()
                            .stream()
                            .filter(allergen -> !allergensToIngredients.containsKey(allergen))
                            .collect(Collectors.toSet());
                    final Set<String> ingredients2 = foods.get(j).getIngredients()
                            .stream()
                            .filter(ingredient -> !ingredientsToAllergens.containsKey(ingredient))
                            .collect(Collectors.toSet());
                    final Set<String> allergens2 = foods.get(j).getAllergens()
                            .stream()
                            .filter(allergen -> !allergensToIngredients.containsKey(allergen))
                            .collect(Collectors.toSet());
                    final Set<String> commonIngredients = Sets.intersection(ingredients1, ingredients2);
                    final Set<String> commonAllergens = Sets.intersection(allergens1, allergens2);
                    if (ingredients1.size() == 1 && allergens1.size() == 1) {
                        final String ingredient = ingredients1.iterator().next();
                        final String allergen = allergens1.iterator().next();
                        ingredientsToAllergens.put(ingredient, allergen);
                        allergensToIngredients.put(allergen, ingredient);
                        changes++;
                    }
                    if (ingredients2.size() == 1 && allergens2.size() == 1) {
                        final String ingredient = ingredients2.iterator().next();
                        final String allergen = allergens2.iterator().next();
                        ingredientsToAllergens.put(ingredient, allergen);
                        allergensToIngredients.put(allergen, ingredient);
                        changes++;
                    }
                    if (commonIngredients.size() == 1 && commonAllergens.size() == 1) {
                        final String commonIngredient = commonIngredients.iterator().next();
                        final String commonAllergen = commonAllergens.iterator().next();
                        ingredientsToAllergens.put(commonIngredient, commonAllergen);
                        allergensToIngredients.put(commonAllergen, commonIngredient);
                        changes++;
                    }
                    if (commonAllergens.size() == 1) {
                        final String commonAllergen = commonAllergens.iterator().next();
                        if (!allergensToPossibleIngredients.containsKey(commonAllergen)) {
                            allergensToPossibleIngredients.put(commonAllergen, commonIngredients);
                        } else {
                            allergensToPossibleIngredients.put(
                                    commonAllergen,
                                    Sets.intersection(
                                            allergensToPossibleIngredients.get(commonAllergen),
                                            commonIngredients));
                        }
                    }
                }
            }
            for (final Map.Entry<String, Set<String>> entry : allergensToPossibleIngredients.entrySet()) {
                final String allergen = entry.getKey();
                final Set<String> possibleIngredients = entry.getValue();
                if (!allergensToIngredients.containsKey(allergen) && possibleIngredients.size() == 1) {
                    final String ingredient = possibleIngredients.iterator().next();
                    ingredientsToAllergens.put(ingredient, allergen);
                    allergensToIngredients.put(allergen, ingredient);
                    changes++;
                }
            }
            if (changes == 0) done = true;
        }
        return ingredientsToAllergens;
    }

    @Getter
    @AllArgsConstructor
    static class Food {
        private final Set<String> ingredients;
        private final Set<String> allergens;
    }
}
