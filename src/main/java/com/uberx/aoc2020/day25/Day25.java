package com.uberx.aoc2020.day25;

import org.apache.commons.lang3.tuple.Pair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;

public class Day25 {
    public static Pair<Integer, Integer> getPublicKeys(final String filename) throws IOException {
        try (final BufferedReader reader = new BufferedReader(
                new InputStreamReader(Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename))))) {
            return Pair.of(Integer.parseInt(reader.readLine()), Integer.parseInt(reader.readLine()));
        }
    }

    public long encryptionKey(final int publicKey1,
                              final int publicKey2) {
        final int subjectNumber = 7;
        final int loopSize1 = getLoopSize(publicKey1, subjectNumber);
        return transform(publicKey2, loopSize1);
    }

    private long transform(final int subjectNumber,
                           final int loopSize) {
        long result = 1;
        for (int i = 0; i < loopSize; i++) {
            result *= subjectNumber;
            result %= 20201227;
        }
        return result;
    }

    private int getLoopSize(final int publicKey,
                            final int subjectNumber) {
        long number = 1;

        int cardLoopSize = 0;
        while (number != publicKey) {
            number *= subjectNumber;
            number %= 20201227;
            cardLoopSize++;
        }
        return cardLoopSize;
    }
}
