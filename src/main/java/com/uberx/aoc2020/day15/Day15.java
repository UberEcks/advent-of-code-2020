package com.uberx.aoc2020.day15;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.mutable.MutableInt;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class Day15 {
    public static List<Integer> getNumbers(final String filename) throws IOException {
        try (final BufferedReader reader = new BufferedReader(
                new InputStreamReader(Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename))))) {
            final String line = reader.readLine();
            return Arrays.stream(line.split(","))
                    .map(Integer::parseInt)
                    .collect(Collectors.toList());
        }
    }

    public int spokenNumber(final List<Integer> numbers,
                            final int requiredIndex) {
        final Map<Integer, List<Integer>> history = new HashMap<>();
        for (int i = 0; i < numbers.size(); i++) {
            final int index = i;
            history.compute(numbers.get(i), (k, v) -> {
                if (v == null) {
                    return Lists.newArrayList(index);
                }
                v.add(index);
                return v;
            });
        }

        final MutableInt currentIndex = new MutableInt(numbers.size());
        int previousNumber = numbers.get(numbers.size() - 1);
        while (currentIndex.getValue() < requiredIndex) {
            if (history.get(previousNumber).size() == 1) {
                previousNumber = 0;
            } else {
                final List<Integer> previousIndices = history.get(previousNumber);
                previousNumber = previousIndices.get(previousIndices.size() - 1)
                        - previousIndices.get(previousIndices.size() - 2);
            }
            history.compute(previousNumber, (k, v) -> {
                if (v == null) {
                    return Lists.newArrayList(currentIndex.getValue());
                }
                v.add(currentIndex.getValue());
                return v;
            });
            currentIndex.add(1);
        }
        return previousNumber;
    }
}
