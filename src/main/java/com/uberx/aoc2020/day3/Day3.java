package com.uberx.aoc2020.day3;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;

import java.awt.*;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

public class Day3 {
    private static final char TREE = '#';

    public static Character[][] getGrid(final String filename) throws IOException {
        return IOUtils
                .readLines(
                        Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                        StandardCharsets.UTF_8)
                .stream()
                .map(line -> line.chars()
                        .mapToObj(c -> (char) c)
                        .toArray(Character[]::new))
                .toArray(Character[][]::new);
    }

    public long treeEncounters(final Character[][] grid,
                               final ImmutablePair<Integer, Integer> slope) {
        final Point currentPosition = new Point(0, 0);
        int encounters = 0;
        while (currentPosition.x < grid.length) {
            currentPosition.translate(slope.getLeft(), slope.getRight());
            currentPosition.y %= grid[0].length;
            if (currentPosition.x < grid.length && grid[currentPosition.x][currentPosition.y] == TREE) {
                encounters++;
            }
        }
        return encounters;
    }
}
