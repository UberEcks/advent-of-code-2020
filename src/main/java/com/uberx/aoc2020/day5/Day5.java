package com.uberx.aoc2020.day5;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Day5 {
    public static List<String> getBoardingPasses(final String filename) throws IOException {
        return IOUtils.readLines(
                Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                StandardCharsets.UTF_8);
    }

    public List<Integer> getSeatIds(final List<String> boardingPasses) {
        return boardingPasses.stream()
                .map(boardingPass ->
                        8 * obtainSeatCoordinateFromBoardingPass(boardingPass.substring(0, 7), 128, 'F') +
                                obtainSeatCoordinateFromBoardingPass(boardingPass.substring(7), 8, 'L'))
                .collect(Collectors.toList());
    }

    public Integer maxSeatId(final List<Integer> seatIds) {
        return seatIds.stream()
                .mapToInt(Integer::intValue)
                .max()
                .getAsInt();
    }

    public Integer missingSeatId(final List<Integer> seatIds) {
        final List<Integer> sortedSeatIds = seatIds
                .stream()
                .sorted()
                .collect(Collectors.toList());
        for (int i = 1; i < sortedSeatIds.size() - 1; i++) {
            if (sortedSeatIds.get(i) != sortedSeatIds.get(i - 1) + 1) {
                return sortedSeatIds.get(i - 1) + 1;
            }
        }
        return null;
    }

    private Integer obtainSeatCoordinateFromBoardingPass(final String boardingPass,
                                                         int possibilities,
                                                         final char lowerHalfCheck) {
        int left = 0;
        int right = possibilities - 1;
        for (int i = 0; i < boardingPass.length() - 1; i++) {
            possibilities /= 2;
            if (boardingPass.charAt(i) == lowerHalfCheck) {
                right -= possibilities;
            } else {
                left += possibilities;
            }
        }
        return boardingPass.charAt(boardingPass.length() - 1) == lowerHalfCheck ? left : right;
    }
}
