package com.uberx.aoc2020.day9;

import com.uberx.aoc2020.day1.Day1;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class Day9 {
    public static List<Long> getInput(final String filename) throws IOException {
        return IOUtils
                .readLines(
                        Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                        StandardCharsets.UTF_8)
                .stream()
                .map(Long::parseLong)
                .collect(Collectors.toList());
    }

    public Pair<Long, Integer> invalidNumber(final List<Long> numbers,
                                             final int preambleLength) {
        // Re-use Day 1 code!
        final Day1 day1 = new Day1();
        for (int i = preambleLength + 1; i < numbers.size(); i++) {
            final Pair<Long, Long> sumPair = day1.pairWithSum(
                    numbers.subList(i - (preambleLength + 1), i).toArray(new Long[0]), numbers.get(i));
            if (sumPair == null) {
                return Pair.of(numbers.get(i), i);
            }
        }
        throw new RuntimeException("No invalid number found");
    }

    public long encryptionWeakness(final List<Long> numbers,
                                   final Pair<Long, Integer> invalidNumber) {
        final List<Long> relevantNumbers = numbers.subList(0, invalidNumber.getValue());

        final Map<Long, Integer> sums = new HashMap<>();
        long numberToFind = invalidNumber.getKey();
        long runningSum = 0;
        for (int i = 0; i < relevantNumbers.size(); i++) {
            runningSum += relevantNumbers.get(i);
            sums.put(runningSum, i);
            if (sums.containsKey(runningSum - numberToFind) && sums.get(runningSum - numberToFind) < i - 1) {
                final List<Long> contiguousNumbers = relevantNumbers.subList(
                        sums.get(runningSum - numberToFind) + 1, i + 1);
                Collections.sort(contiguousNumbers);
                return contiguousNumbers.get(0) + contiguousNumbers.get(contiguousNumbers.size() - 1);
            }
        }
        throw new RuntimeException("No weakness found in encryption");
    }
}
