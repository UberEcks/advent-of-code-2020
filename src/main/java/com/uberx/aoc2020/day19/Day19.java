package com.uberx.aoc2020.day19;

import com.google.common.collect.Sets;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class Day19 {
    public static Input getInput(final String filename) throws IOException {
        try (final BufferedReader reader = new BufferedReader(
                new InputStreamReader(Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename))))) {
            String line;
            // rules
            final Map<String, Rule> rules = new HashMap<>();
            while (!(line = reader.readLine()).isEmpty()) {
                final String[] colonSeparatedValues = line.split(":");
                rules.put(colonSeparatedValues[0], new Rule(colonSeparatedValues[1].trim()));
            }
            // messages
            final List<String> messages = new ArrayList<>();
            while ((line = reader.readLine()) != null) {
                messages.add(line);
            }

            return new Input(messages, rules);
        }
    }

    public long messagesThatMatchRule1(final List<String> messages,
                                       final Map<String, Rule> rules,
                                       final Rule rule) {
        final Map<String, Set<String>> cache = new HashMap<>();
        final Set<String> rulePossibilities = rule.getPossibilities(rules, cache);
        return messages.stream()
                .filter(rulePossibilities::contains)
                .count();
    }

    public long messagesThatMatchRule2(final List<String> messages,
                                       final Map<String, Rule> rules,
                                       final Rule rule) {
        final Map<String, Set<String>> cache = new HashMap<>();
        final Set<String> rulePossibilities = rule.getPossibilities(rules, cache);
        final List<String> matches = messages.stream()
                .filter(message -> rulePossibilities.contains(message) || matchesModifiedRule8Or11(message, cache))
                .collect(Collectors.toList());
        return matches.size();
    }

    private boolean matchesModifiedRule8Or11(final String message,
                                             final Map<String, Set<String>> cache) {
        final Set<String> rule42Possibilities = cache.get("42");
        final Set<String> rule31Possibilities = cache.get("31");
        final int rule42MatchLength = rule42Possibilities.iterator().next().length();
        final int rule31MatchLength = rule31Possibilities.iterator().next().length();

        if (message.length() % rule42MatchLength != 0
                && message.length() % (rule42MatchLength + rule31MatchLength) != 0) {
            return false;
        }

        int index = 0;
        int rule42Matches = 0;
        while (index <= message.length() - rule42MatchLength
                && rule42Possibilities.contains(message.substring(index, index + rule42MatchLength))) {
            rule42Matches++;
            index += rule42MatchLength;
        }

        int rule31Matches = 0;
        while (index <= message.length() - rule31MatchLength
                && rule31Possibilities.contains(message.substring(index, index + rule31MatchLength))) {
            rule31Matches++;
            index += rule31MatchLength;
        }

        return rule31Matches > 0 && rule42Matches > rule31Matches
                && rule42Matches * rule42MatchLength + rule31Matches * rule31MatchLength == message.length();
    }

    @Getter
    @AllArgsConstructor
    static class Input {
        final List<String> messages;
        final Map<String, Rule> rules;
    }

    @Getter
    @AllArgsConstructor
    static class Rule {
        final String expression;

        Set<String> getPossibilities(final Map<String, Rule> rules,
                                     final Map<String, Set<String>> cache) {
            final String[] tokens = expression.split(" ");
            if (tokens.length == 1 && tokens[0].startsWith("\"")) {
                return Sets.newHashSet(expression.replace("\"", ""));
            } else if (tokens.length == 1) {
                final String rule = tokens[0];
                return rules.get(rule).getPossibilities(rules, cache);
            } else if (tokens.length == 2) {
                final String rule1 = tokens[0];
                final String rule2 = tokens[1];
                return getPossibilities(rules, cache, rule1, rule2);
            } else if (tokens.length == 3 && !tokens[1].equals("|")) {
                final String rule1 = tokens[0];
                final String rule2 = tokens[1];
                final String rule3 = tokens[2];
                return getConcatenatedPossibilities(
                        getPossibilities(rules, cache, rule1, rule2),
                        rules.get(rule3).getPossibilities(rules, cache),
                        rule1 + " " + rule2,
                        rule3,
                        cache);
            } else if (tokens.length == 3) {
                final String rule1 = tokens[0];
                final String rule2 = tokens[2];
                return Sets.union(
                        rules.get(rule1).getPossibilities(rules, cache),
                        rules.get(rule2).getPossibilities(rules, cache));
            }
            final String rule1 = tokens[0];
            final String rule2 = tokens[1];
            final String rule3 = tokens[3];
            final String rule4 = tokens[4];
            return Sets.union(
                    getPossibilities(rules, cache, rule1, rule2),
                    getPossibilities(rules, cache, rule3, rule4));
        }

        private Set<String> getPossibilities(final Map<String, Rule> rules,
                                             final Map<String, Set<String>> cache,
                                             final String rule1,
                                             final String rule2) {
            if (cache.containsKey(rule1 + " " + rule2)) {
                return cache.get(rule1 + " " + rule2);
            }
            final Set<String> rule1Possibilities = cache.getOrDefault(
                    rule1,
                    cache.getOrDefault(rule1, rules.get(rule1).getPossibilities(rules, cache)));
            final Set<String> rule2Possibilities = cache.getOrDefault(
                    rule2,
                    cache.getOrDefault(rule2, rules.get(rule2).getPossibilities(rules, cache)));
            cache.putIfAbsent(rule1, rule1Possibilities);
            cache.putIfAbsent(rule2, rule2Possibilities);
            return getConcatenatedPossibilities(rule1Possibilities, rule2Possibilities, rule1, rule2, cache);
        }

        private Set<String> getConcatenatedPossibilities(final Set<String> possibilities1,
                                                         final Set<String> possibilities2,
                                                         final String rule1,
                                                         final String rule2,
                                                         final Map<String, Set<String>> cache) {
            if (cache.containsKey(rule1 + " " + rule2)) {
                return cache.get(rule1 + " " + rule2);
            }
            final Set<String> concatenatedPossibilities = new HashSet<>();
            possibilities1.forEach(
                    rule1Possibility -> possibilities2.forEach(
                            rule2Possibility -> concatenatedPossibilities.add(rule1Possibility + rule2Possibility)));
            cache.putIfAbsent(rule1 + " " + rule2, concatenatedPossibilities);
            return concatenatedPossibilities;
        }
    }
}
