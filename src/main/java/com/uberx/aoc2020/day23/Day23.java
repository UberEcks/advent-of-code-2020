package com.uberx.aoc2020.day23;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

public class Day23 {
    public static CyclicalDoublyLinkedList getInput(final String filename) throws IOException {
        final CyclicalDoublyLinkedList input = new CyclicalDoublyLinkedList();
        try (final BufferedReader reader = new BufferedReader(
                new InputStreamReader(Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename))))) {
            final String line = reader.readLine();
            Arrays.stream(line.split(""))
                    .forEach(number -> input.add(Integer.parseInt(number)));
        }
        input.makeCircular();
        return input;
    }

    public static CyclicalDoublyLinkedList getInput(final String filename,
                                                    final int cups) throws IOException {
        final CyclicalDoublyLinkedList input = new CyclicalDoublyLinkedList();
        try (final BufferedReader reader = new BufferedReader(
                new InputStreamReader(Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename))))) {
            final String line = reader.readLine();
            Arrays.stream(line.split(""))
                    .forEach(number -> input.add(Integer.parseInt(number)));
        }
        for (int i = 10; i <= cups; i++) {
            input.add(i);
        }
        input.makeCircular();
        return input;
    }

    public String labelsAfterCup1(final CyclicalDoublyLinkedList input,
                                  final int moves) {
        playCrabCups(input, moves);
        return input.toString(1, "").substring(1);
    }

    public long productOf2ImmediateLabelsAfterCup1(final CyclicalDoublyLinkedList input,
                                                   final int moves) {
        playCrabCups(input, moves);
        final Node one = input.get(1);
        return (long) one.next.value * one.next.next.value;
    }

    private void playCrabCups(final CyclicalDoublyLinkedList input,
                              final int moves) {
        Node current = input.getFirst();
        for (int i = 0; i < moves; i++) {
            final int[] removedValues = input.removeAfter(current.value, 3);
            int destinationValue = current.value - 1;
            while (!input.contains(destinationValue)) {
                destinationValue--;
                if (destinationValue < 1) {
                    destinationValue = input.getMaxValue();
                }
            }
            input.addAfter(destinationValue, removedValues);
            current = current.next;
        }
    }

    static class CyclicalDoublyLinkedList {
        Node head;
        Node tail;
        Map<Integer, Node> lookup = new LinkedHashMap<>();
        int maxValue = -1;

        CyclicalDoublyLinkedList() {
            this.head = new Node(Integer.MIN_VALUE);
            this.tail = new Node(Integer.MAX_VALUE);
            head.next = tail;
            tail.previous = head;
        }

        Node getFirst() {
            return lookup.entrySet().iterator().next().getValue();
        }

        Node get(final int value) {
            return lookup.get(value);
        }

        boolean contains(final int value) {
            return lookup.containsKey(value);
        }

        int getMaxValue() {
            return maxValue;
        }

        void add(final int newValue) {
            addBefore(newValue, tail);
        }

        void addAfter(final int existingValue, final int[] newValues) {
            final Node existingNode = lookup.get(existingValue);
            final Node existingNextNode = existingNode.next;
            for (final int newValue : newValues) {
                addBefore(newValue, existingNextNode);
            }
        }

        private void addBefore(final int newValue, final Node node) {
            final Node toAdd = new Node(newValue, node.previous, node);
            toAdd.previous.next = toAdd;
            node.previous = toAdd;
            lookup.put(newValue, toAdd);
        }

        void makeCircular() {
            head.next.previous = tail.previous;
            tail.previous.next = head.next;
            head = null;
            tail = null;
            maxValue = lookup.size();
        }

        int[] removeAfter(final int value, final int numValues) {
            final int[] removedValues = new int[numValues];
            final Node currentNode = lookup.get(value);
            for (int i = 0; i < numValues; i++) {
                final int toRemove = currentNode.next.value;
                remove(toRemove);
                removedValues[i] = toRemove;
            }
            return removedValues;
        }

        private void remove(final int value) {
            Node toRemove = lookup.get(value);
            toRemove.previous.next = toRemove.next;
            toRemove.next.previous = toRemove.previous;
            lookup.remove(value);
        }

        @Override
        public String toString() {
            return toString(lookup.keySet().iterator().next(), " ");
        }

        String toString(final int value,
                        final String delimiter) {
            Node currentNode = lookup.get(value);
            final String[] values = new String[lookup.size()];
            values[0] = String.valueOf(value);
            int index = 1;
            while (currentNode.next.value != value) {
                currentNode = currentNode.next;
                values[index++] = String.valueOf(currentNode.value);
            }
            return String.join(delimiter, values);
        }
    }

    @RequiredArgsConstructor
    @AllArgsConstructor
    static class Node {
        final int value;
        Node previous;
        Node next;
    }
}
