package com.uberx.aoc2020.day22;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;

public class Day22 {
    public static Combat getPlayerCards(final String filename) throws IOException {
        final LinkedList<Integer> player1Cards = new LinkedList<>();
        final LinkedList<Integer> player2Cards = new LinkedList<>();
        try (final BufferedReader reader = new BufferedReader(
                new InputStreamReader(Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename))))) {
            String line;

            reader.readLine(); // Player 1
            while (!(line = reader.readLine()).isEmpty()) {
                player1Cards.add(Integer.parseInt(line));
            }

            reader.readLine(); // Player 2
            while ((line = reader.readLine()) != null) {
                player2Cards.add(Integer.parseInt(line));
            }
        }
        return new Combat(player1Cards, player2Cards);
    }

    public long winningScore1(final Combat combat) {
        return winningScore(combat, this::play1);
    }

    public long winningScore2(final Combat combat) {
        return winningScore(combat, this::play2);
    }

    private long winningScore(final Combat combat,
                              final Function<Combat, Combat.Player> playFunction) {
        final Combat.Player winner = playFunction.apply(combat);

        final LinkedList<Integer> player1Cards = combat.getPlayer1Cards();
        final LinkedList<Integer> player2Cards = combat.getPlayer2Cards();
        final LinkedList<Integer> winningCards = winner == Combat.Player.PLAYER2 ? player2Cards : player1Cards;
        long score = 0;
        for (int i = winningCards.size(); i > 0; i--) {
            score += (long) i * winningCards.poll();
        }
        return score;
    }

    private Combat.Player play1(final Combat combat) {
        return play(combat, this::getWinner1);
    }

    private Combat.Player play2(final Combat combat) {
        return play(combat, this::getWinner2);
    }

    private Combat.Player play(final Combat combat,
                               final Function<Object[], Combat.Player> winFunction) {
        final Set<String> previousRounds = new HashSet<>();
        final LinkedList<Integer> player1Cards = combat.getPlayer1Cards();
        final LinkedList<Integer> player2Cards = combat.getPlayer2Cards();
        while (!(player1Cards.isEmpty() || player2Cards.isEmpty())) {
            if (previousRounds.contains(player1Cards.toString() + player2Cards.toString())) {
                return Combat.Player.PLAYER1;
            }
            previousRounds.add(player1Cards.toString() + player2Cards.toString());
            final int player1Card = player1Cards.poll();
            final int player2Card = player2Cards.poll();
            final Combat.Player roundWinner = winFunction.apply(
                    new Object[]{player1Card, player2Card, player1Cards, player2Cards});
            if (roundWinner == Combat.Player.PLAYER1) {
                player1Cards.add(player1Card);
                player1Cards.add(player2Card);
            } else {
                player2Cards.add(player2Card);
                player2Cards.add(player1Card);
            }
        }
        return player1Cards.isEmpty() ? Combat.Player.PLAYER2 : Combat.Player.PLAYER1;
    }

    private Combat.Player getWinner1(final Object[] args) {
        final int player1Card = (int) args[0];
        final int player2Card = (int) args[1];
        return player1Card > player2Card ? Combat.Player.PLAYER1 : Combat.Player.PLAYER2;
    }

    @SuppressWarnings("unchecked")
    private Combat.Player getWinner2(final Object[] args) {
        final int player1Card = (int) args[0];
        final int player2Card = (int) args[1];
        final LinkedList<Integer> player1Cards = (LinkedList<Integer>) args[2];
        final LinkedList<Integer> player2Cards = (LinkedList<Integer>) args[3];
        if (player1Cards.size() >= player1Card && player2Cards.size() >= player2Card) {
            return play(
                    new Combat(
                            new LinkedList<>(player1Cards.subList(0, player1Card)),
                            new LinkedList<>(player2Cards.subList(0, player2Card))),
                    this::getWinner2);
        }
        return getWinner1(args);
    }

    @Getter
    @AllArgsConstructor
    static class Combat {
        private final LinkedList<Integer> player1Cards;
        private final LinkedList<Integer> player2Cards;

        enum Player {
            PLAYER1, PLAYER2
        }
    }
}
