package com.uberx.aoc2020.day12;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.io.IOUtils;

import java.awt.*;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class Day12 {
    public static List<Navigation> getNavigationInstructions(final String filename) throws IOException {
        return IOUtils
                .readLines(
                        Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                        StandardCharsets.UTF_8)
                .stream()
                .map(line -> new Navigation(line.charAt(0), Integer.parseInt(line.substring(1))))
                .collect(Collectors.toList());
    }

    public int manhattanDistance1(final List<Navigation> navigationInstructions) {
        final Ship ship = new Ship('E', new Point(0, 0));
        for (final Navigation navigationInstruction : navigationInstructions) {
            ship.apply1(navigationInstruction);
        }
        return Math.abs(ship.getShipLocation().x) + Math.abs(ship.getShipLocation().y);
    }

    public int manhattanDistance2(final List<Navigation> navigationInstructions) {
        final Ship ship = new Ship('E', new Point(0, 0), new Point(10, 1));
        for (final Navigation navigationInstruction : navigationInstructions) {
            ship.apply2(navigationInstruction);
        }
        return Math.abs(ship.getShipLocation().x) + Math.abs(ship.getShipLocation().y);
    }

    @Getter
    @AllArgsConstructor
    static class Navigation {
        private final char action;
        private final int value;
    }

    @Getter
    static class Ship {
        private static final Set<Character> MOVE_DIRECTIONS = Sets.newHashSet('N', 'S', 'E', 'W');
        private static final Set<Character> ROTATE_DIRECTIONS = Sets.newHashSet('L', 'R');
        private static final List<Character> CLOCKWISE_DIRECTIONS = Lists.newArrayList('N', 'E', 'S', 'W');
        private char facingDirection;
        private final Point shipLocation;
        private final Point waypointLocation;

        public Ship(final char facingDirection,
                    final Point shipLocation) {
            this(facingDirection, shipLocation, null);
        }

        public Ship(final char facingDirection,
                    final Point shipLocation,
                    final Point waypointLocation) {
            this.facingDirection = facingDirection;
            this.shipLocation = shipLocation;
            this.waypointLocation = waypointLocation;
        }

        public void apply1(final Navigation navigation) {
            if (MOVE_DIRECTIONS.contains(navigation.action)) {
                move(shipLocation, navigation.action, navigation.value);
            } else if (navigation.action == 'F') {
                move(shipLocation, facingDirection, navigation.value);
            } else if (ROTATE_DIRECTIONS.contains(navigation.action)) {
                rotate1(navigation.action, navigation.value);
            }
        }

        public void apply2(final Navigation navigation) {
            if (MOVE_DIRECTIONS.contains(navigation.action)) {
                move(waypointLocation, navigation.action, navigation.value);
            } else if (navigation.action == 'F') {
                final int waypointDx = waypointLocation.x - shipLocation.x;
                final int waypointDy = waypointLocation.y - shipLocation.y;
                shipLocation.translate(navigation.value * waypointDx, navigation.value * waypointDy);
                waypointLocation.setLocation(shipLocation.x + waypointDx, shipLocation.y + waypointDy);
            } else if (ROTATE_DIRECTIONS.contains(navigation.action)) {
                rotate2(navigation.action, navigation.value);
            }
        }

        private void move(final Point location,
                          final char moveDirection,
                          final int value) {
            if (moveDirection == 'N') {
                location.translate(0, value);
            } else if (moveDirection == 'S') {
                location.translate(0, -value);
            } else if (moveDirection == 'E') {
                location.translate(value, 0);
            } else if (moveDirection == 'W') {
                location.translate(-value, 0);
            }
        }

        private void rotate1(final char rotateDirection,
                             final int degrees) {
            final int rotations = degrees / 90 * (rotateDirection == 'L' ? -1 : 1);
            final int currentIndex = CLOCKWISE_DIRECTIONS.indexOf(facingDirection);
            final int destinationIndex = currentIndex + rotations;
            if (destinationIndex < 0) {
                facingDirection = CLOCKWISE_DIRECTIONS.get(CLOCKWISE_DIRECTIONS.size() + destinationIndex);
            } else {
                facingDirection = CLOCKWISE_DIRECTIONS.get(destinationIndex % CLOCKWISE_DIRECTIONS.size());
            }
        }

        private void rotate2(final char rotateDirection,
                             final int degrees) {
            final int angle = rotateDirection == 'L' ? degrees : -degrees;
            final double sine = Math.sin(Math.toRadians(angle));
            final double cosine = Math.cos(Math.toRadians(angle));

            // Translate waypoint to origin
            waypointLocation.translate(-shipLocation.x, -shipLocation.y);

            // Rotate waypoint
            final double newX = waypointLocation.x * cosine - waypointLocation.y * sine;
            final double newY = waypointLocation.x * sine + waypointLocation.y * cosine;
            waypointLocation.setLocation(newX, newY);

            // Translate waypoint back
            waypointLocation.translate(shipLocation.x, shipLocation.y);
        }
    }
}
