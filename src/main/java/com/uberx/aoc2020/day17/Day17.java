package com.uberx.aoc2020.day17;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.tuple.Pair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Day17 {
    private static final char ACTIVE = '#';
    private static final char INACTIVE = '.';

    public static Map<List<Integer>, Character> getInitialState(final String filename,
                                                                final int dimensions)
            throws IOException {
        final Map<List<Integer>, Character> initialState = new HashMap<>();
        try (final BufferedReader reader = new BufferedReader(
                new InputStreamReader(Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename))))) {
            String line;
            int x = 0;
            while ((line = reader.readLine()) != null) {
                final String[] states = line.split("");
                for (int y = 0, statesLength = states.length; y < statesLength; y++) {
                    final String state = states[y];
                    final List<Integer> coordinates = Lists.newArrayList(x, y);
                    coordinates.addAll(IntStream.range(0, dimensions - 2)
                            .mapToObj(i -> 0)
                            .collect(Collectors.toList()));
                    initialState.put(coordinates, state.charAt(0));
                }
                x++;
            }
        }
        return initialState;
    }

    public long numActive1(final Map<List<Integer>, Character> state,
                           final int cycles) {
        return numActive(
                state,
                cycles,
                this::getNeighbors1,
                pair -> getActiveNeighborCount1(pair.getLeft(), pair.getRight()));
    }

    public long numActive2(final Map<List<Integer>, Character> state,
                           final int cycles) {
        return numActive(
                state,
                cycles,
                this::getNeighbors2,
                pair -> getActiveNeighborCount2(pair.getLeft(), pair.getRight()));
    }

    private long numActive(final Map<List<Integer>, Character> state,
                           final int cycles,
                           final Function<List<Integer>, Set<List<Integer>>> getNeighborsFunction,
                           final Function<Pair<Map<List<Integer>, Character>, List<Integer>>, Long> getActiveNeighborCountFunction) {
        for (int cycle = 0; cycle < cycles; cycle++) {
            final Map<List<Integer>, Character> newCubes = new HashMap<>();
            for (final List<Integer> currentCube : state.keySet()) {
                final Set<List<Integer>> neighbors = getNeighborsFunction.apply(currentCube);
                final Map<List<Integer>, Character> uniqueCubeEntries = neighbors.stream()
                        .filter(cube -> !state.containsKey(cube))
                        .collect(Collectors.toMap(cube -> cube, cube -> INACTIVE));
                newCubes.putAll(uniqueCubeEntries);
            }
            state.putAll(newCubes);

            final Map<List<Integer>, Character> newState = new HashMap<>();
            for (final Map.Entry<List<Integer>, Character> cubeEntry : state.entrySet()) {
                final long activeNeighborCount = getActiveNeighborCountFunction.apply(
                        Pair.of(state, cubeEntry.getKey()));
                if (cubeEntry.getValue() == ACTIVE && activeNeighborCount != 2 && activeNeighborCount != 3) {
                    newState.put(cubeEntry.getKey(), INACTIVE);
                } else if (cubeEntry.getValue() == INACTIVE && activeNeighborCount == 3) {
                    newState.put(cubeEntry.getKey(), ACTIVE);
                } else {
                    newState.put(cubeEntry.getKey(), cubeEntry.getValue());
                }
            }
            state.putAll(newState);
        }
        return getActiveCount(state);
    }

    private long getActiveCount(final Map<List<Integer>, Character> state) {
        return state.values()
                .stream()
                .filter(status -> status == ACTIVE)
                .count();
    }

    private long getActiveNeighborCount1(final Map<List<Integer>, Character> state,
                                         final List<Integer> cube) {
        final Set<List<Integer>> neighbors = getNeighbors1(cube);
        return neighbors.stream()
                .map(neighbor -> state.getOrDefault(neighbor, INACTIVE))
                .filter(status -> status == ACTIVE)
                .count();
    }

    private long getActiveNeighborCount2(final Map<List<Integer>, Character> state,
                                         final List<Integer> cube) {
        final Set<List<Integer>> neighbors = getNeighbors2(cube);
        return neighbors.stream()
                .map(neighbor -> state.getOrDefault(neighbor, INACTIVE))
                .filter(status -> status == ACTIVE)
                .count();
    }

    private Set<List<Integer>> getNeighbors1(final List<Integer> cube) {
        final Set<List<Integer>> neighbors = new HashSet<>();
        for (int x = -1; x <= 1; x++) {
            for (int y = -1; y <= 1; y++) {
                for (int z = -1; z <= 1; z++) {
                    if (x != 0 || y != 0 || z != 0) {
                        neighbors.add(Lists.newArrayList(cube.get(0) + x, cube.get(1) + y, cube.get(2) + z));
                    }
                }
            }
        }
        return neighbors;
    }

    private Set<List<Integer>> getNeighbors2(final List<Integer> cube) {
        final Set<List<Integer>> neighbors = new HashSet<>();
        for (int x = -1; x <= 1; x++) {
            for (int y = -1; y <= 1; y++) {
                for (int z = -1; z <= 1; z++) {
                    for (int w = -1; w <= 1; w++) {
                        if (x != 0 || y != 0 || z != 0 || w != 0) {
                            neighbors.add(
                                    Lists.newArrayList(
                                            cube.get(0) + x,
                                            cube.get(1) + y,
                                            cube.get(2) + z,
                                            cube.get(3) + w));
                        }
                    }
                }
            }
        }
        return neighbors;
    }
}
