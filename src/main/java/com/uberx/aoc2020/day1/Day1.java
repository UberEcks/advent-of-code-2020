package com.uberx.aoc2020.day1;

import com.google.common.collect.Lists;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class Day1 {
    public static Long[] getExpenseEntries(final String filename) throws IOException {
        return IOUtils
                .readLines(
                        Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                        StandardCharsets.UTF_8)
                .stream()
                .map(Long::valueOf)
                .toArray(Long[]::new);
    }

    public Pair<Long, Long> pairWithSum(final Long[] expenseEntries,
                                        final long sum) {
        return pairWithSum(expenseEntries, 0, sum);
    }

    public Pair<Long, Long> pairWithSum(final Long[] expenseEntries,
                                        final int startIndex,
                                        final long sum) {
        final Set<Long> seenEntries = new HashSet<>();
        for (int i = startIndex; i < expenseEntries.length; i++) {
            final long currentExpense = expenseEntries[i];
            if (seenEntries.contains(sum - currentExpense)) {
                return Pair.of(currentExpense, sum - currentExpense);
            }
            seenEntries.add(currentExpense);
        }
        return null;
    }

    public List<Long> tripletWithSum(final Long[] expenseEntries,
                                     final long sum) {
        for (int i = 0; i < expenseEntries.length; i++) {
            final long currentExpense = expenseEntries[i];
            // Swap with first element
            expenseEntries[i] = expenseEntries[0];
            expenseEntries[0] = currentExpense;
            // Find a pair for the remaining part of the sum
            final long remaining = sum - currentExpense;
            final Pair<Long, Long> pair = pairWithSum(expenseEntries, 1, remaining);
            if (pair != null) {
                return Lists.newArrayList(currentExpense, pair.getLeft(), pair.getRight());
            }
        }
        return null;
    }
}
