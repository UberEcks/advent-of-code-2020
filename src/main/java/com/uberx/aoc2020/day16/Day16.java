package com.uberx.aoc2020.day16;

import com.google.common.collect.Sets;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import org.apache.commons.lang3.tuple.Pair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Day16 {
    public static Ticket getTicket(final String filename) throws IOException {
        final List<Rule> rules = new ArrayList<>();
        final List<Integer> yourTicket;
        final List<List<Integer>> nearbyTickets = new ArrayList<>();

        try (final BufferedReader reader = new BufferedReader(
                new InputStreamReader(Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename))))) {
            String line;

            // rules
            while (!(line = reader.readLine()).isEmpty()) {
                final String[] colonSeparatedValues = line.split(":");
                final String name = colonSeparatedValues[0];

                final String[] orSeparatedValues = colonSeparatedValues[1].trim().split(" or ");
                final String[] hyphenSeparatedValues1 = orSeparatedValues[0].split("-");
                final String[] hyphenSeparatedValues2 = orSeparatedValues[1].split("-");
                rules.add(
                        new Rule(
                                name,
                                Pair.of(
                                        Integer.parseInt(hyphenSeparatedValues1[0]),
                                        Integer.parseInt(hyphenSeparatedValues1[1])),
                                Pair.of(
                                        Integer.parseInt(hyphenSeparatedValues2[0]),
                                        Integer.parseInt(hyphenSeparatedValues2[1]))));
            }

            // your ticket
            reader.readLine();
            yourTicket = Arrays.stream(reader.readLine().split(","))
                    .map(Integer::parseInt)
                    .collect(Collectors.toList());
            reader.readLine();

            // nearby tickets
            reader.readLine();
            while ((line = reader.readLine()) != null) {
                nearbyTickets.add(Arrays.stream(line.split(","))
                        .map(Integer::parseInt)
                        .collect(Collectors.toList()));
            }
        }

        return new Ticket(rules, yourTicket, nearbyTickets);
    }

    public int ticketErrorRate(final List<Rule> rules,
                               final List<List<Integer>> nearbyTickets) {
        final List<Integer> invalidNumbers = new ArrayList<>();
        for (final List<Integer> nearbyTicket : nearbyTickets) {
            for (final Integer number : nearbyTicket) {
                boolean valid = false;
                for (int i = 0; i < rules.size() && !valid; i++) {
                    Rule rule = rules.get(i);
                    if (rule.test(number)) {
                        valid = true;
                    }
                }
                if (!valid) invalidNumbers.add(number);
            }
        }
        return invalidNumbers.stream()
                .mapToInt(i -> i)
                .sum();
    }

    public Map<String, Integer> ruleNameToIndexMapping(final List<Rule> rules,
                                                       final List<List<Integer>> nearbyTickets) {
        final List<List<Integer>> validTickets = new ArrayList<>();
        for (final List<Integer> nearbyTicket : nearbyTickets) {
            boolean validTicket = true;
            for (int i = 0; i < nearbyTicket.size() && validTicket; i++) {
                final Integer number = nearbyTicket.get(i);
                boolean valid = false;
                for (int j = 0; j < rules.size() && !valid; j++) {
                    Rule rule = rules.get(j);
                    if (rule.test(number)) {
                        valid = true;
                    }
                }
                if (!valid) validTicket = false;
            }
            if (validTicket) {
                validTickets.add(nearbyTicket);
            }
        }

        final Map<Rule, Set<Integer>> invalidIndicesForRule = new LinkedHashMap<>();
        for (final List<Integer> validTicket : validTickets) {
            for (int i = 0; i < validTicket.size(); i++) {
                final Integer number = validTicket.get(i);
                for (final Rule rule : rules) {
                    if (!rule.test(number)) {
                        if (!invalidIndicesForRule.containsKey(rule)) {
                            invalidIndicesForRule.put(rule, new LinkedHashSet<>());
                        }
                        invalidIndicesForRule.get(rule).add(i);
                    }
                }
            }
        }

        final Map<Integer, Rule> ruleForInvalidIndexCount = invalidIndicesForRule.entrySet()
                .stream()
                .collect(Collectors.toMap(entry -> entry.getValue().size(), Map.Entry::getKey));
        final Map<Integer, Rule> ruleForAlreadySolvedIndices = new TreeMap<>();
        final Set<Integer> allRuleIndices = IntStream.range(0, rules.size())
                .boxed()
                .collect(Collectors.toCollection(Sets::newLinkedHashSet));
        for (int i = rules.size() - 1; i > 0; i--) {
            final Rule currentRule = ruleForInvalidIndexCount.get(i);
            final Set<Integer> invalidIndicesForCurrentRule = Sets.union(
                    invalidIndicesForRule.get(currentRule), ruleForAlreadySolvedIndices.keySet());
            final int missingIndex = Sets.difference(allRuleIndices, invalidIndicesForCurrentRule).iterator().next();
            ruleForAlreadySolvedIndices.put(missingIndex, currentRule);
        }

        return ruleForAlreadySolvedIndices.entrySet()
                .stream()
                .collect(Collectors.toMap(entry -> entry.getValue().getName(), Map.Entry::getKey));
    }

    @Getter
    @AllArgsConstructor
    static class Ticket {
        final List<Rule> rules;
        final List<Integer> yourTicket;
        final List<List<Integer>> nearbyTickets;
    }

    @Getter
    @ToString(of = {"name"})
    @AllArgsConstructor
    static class Rule {
        final String name;
        final Pair<Integer, Integer> range1;
        final Pair<Integer, Integer> range2;

        boolean test(final int number) {
            return (number >= range1.getLeft() && number <= range1.getRight())
                    || (number >= range2.getLeft() && number <= range2.getRight());
        }
    }
}
