package com.uberx.aoc2020.day6;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class Day6 {
    public static List<Pair<String, Long>> getUniqueAnswerGroups(final String filename) throws IOException {
        final List<String> input = IOUtils
                .readLines(
                        Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                        StandardCharsets.UTF_8);
        final List<Pair<String, Long>> uniqueAnswerGroups = new ArrayList<>();
        for (int i = 0; i < input.size(); i++) {
            final Map<Character, Integer> uniqueAnswerCounts = new HashMap<>();
            int personCount = 0;
            while (i < input.size() && !input.get(i).isEmpty()) {
                final Set<Character> answers = input.get(i)
                        .chars()
                        .mapToObj(c -> (char) c)
                        .collect(Collectors.toSet());
                answers.forEach(answer ->
                        uniqueAnswerCounts.compute(answer, (key, count) -> (count == null) ? 1 : count + 1));
                i++;
                personCount++;
            }
            final int count = personCount;
            uniqueAnswerGroups.add(
                    Pair.of(uniqueAnswerCounts.keySet()
                                    .stream()
                                    .collect(Collector.of(
                                            StringBuilder::new,
                                            StringBuilder::append,
                                            StringBuilder::append,
                                            StringBuilder::toString)),
                            uniqueAnswerCounts.entrySet()
                                    .stream()
                                    .filter(entry -> entry.getValue() == count)
                                    .count()));
        }
        return uniqueAnswerGroups;
    }

    public int sumOfUniqueAnswerGroupCounts(final List<Pair<String, Long>> uniqueAnswerGroups) {
        return uniqueAnswerGroups
                .stream()
                .mapToInt(pair -> pair.getKey().length())
                .sum();
    }

    public long sumOfUniqueAnswerGroupAllCounts(final List<Pair<String, Long>> uniqueAnswerGroups) {
        return uniqueAnswerGroups
                .stream()
                .mapToLong(Pair::getRight)
                .sum();
    }
}
