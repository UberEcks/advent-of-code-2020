package com.uberx.aoc2020.day13;

import org.apache.commons.lang3.tuple.Pair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Day13 {
    public static Pair<Integer, List<Integer>> getInput(final String filename) throws IOException {
        try (final BufferedReader reader = new BufferedReader(
                new InputStreamReader(Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename))))) {
            return Pair.of(
                    Integer.parseInt(reader.readLine()),
                    Arrays.stream(reader.readLine()
                            .split(","))
                            .map(id -> ("x".equals(id)) ? -1 : Integer.parseInt(id))
                            .collect(Collectors.toList()));
        }
    }

    public Pair<Integer, Integer> earliestTimestampAndBusId(final Pair<Integer, List<Integer>> input) {
        final int timestamp = input.getLeft();
        final List<Integer> busIds = input.getRight();
        final Map<Integer, Integer> earliestTimestampForBus = busIds.stream()
                .filter(id -> id != -1)
                .collect(Collectors.toMap(Function.identity(), id -> timestamp + id - (timestamp % id)));
        final Map.Entry<Integer, Integer> entryWithEarliestTimestamp = Collections.min(
                earliestTimestampForBus.entrySet(),
                Map.Entry.comparingByValue());
        return Pair.of(entryWithEarliestTimestamp.getValue(), entryWithEarliestTimestamp.getKey());
    }

    public long earliestTimestampWithMatchingOffsets(final List<Integer> busIds,
                                                     final long startTimestamp) {
        long timestamp = startTimestamp;
        long increment = 1;
        for (int i = 0; i < busIds.size(); i++) {
            if (busIds.get(i) == -1) {
                continue;
            }
            while ((timestamp + i) % busIds.get(i) != 0) {
                timestamp += increment;
            }
            increment *= busIds.get(i);
        }
        return timestamp;
    }
}
