package com.uberx.aoc2020.day11;

import org.apache.commons.io.IOUtils;

import java.awt.*;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

public class Day11 {
    private static final char EMPTY_SEAT = 'L';
    private static final char OCCUPIED_SEAT = '#';

    public static Character[][] getSeatGrid(final String filename) throws IOException {
        return IOUtils
                .readLines(
                        Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                        StandardCharsets.UTF_8)
                .stream()
                .map(CharSequence::chars)
                .map(intStream -> intStream.mapToObj(c -> (char) c)
                        .toArray(Character[]::new))
                .toArray(Character[][]::new);
    }

    public int numSeatsOccupied(final Character[][] grid,
                                final int rule2NumSeats,
                                final boolean part2) {
        Character[][] currentGrid = grid;
        int changes;
        do {
            final Character[][] newGrid1 = new Character[currentGrid.length][currentGrid[0].length];
            changes = 0;
            // Rule 1 - empty seat check
            for (int i = 0; i < currentGrid.length; i++) {
                for (int j = 0; j < currentGrid[0].length; j++) {
                    if (currentGrid[i][j] == EMPTY_SEAT
                            && getNumOccupiedSeatsAdjacentTo(currentGrid, new Point(i, j), part2) == 0) {
                        newGrid1[i][j] = OCCUPIED_SEAT;
                        changes++;
                    } else {
                        newGrid1[i][j] = currentGrid[i][j];
                    }
                }
            }
            currentGrid = newGrid1;
            // Rule 2 - occupied seat check
            final Character[][] newGrid2 = new Character[currentGrid.length][currentGrid[0].length];
            for (int i = 0; i < currentGrid.length; i++) {
                for (int j = 0; j < currentGrid[0].length; j++) {
                    if (currentGrid[i][j] == OCCUPIED_SEAT
                            && getNumOccupiedSeatsAdjacentTo(currentGrid, new Point(i, j), part2) >= rule2NumSeats) {
                        newGrid2[i][j] = EMPTY_SEAT;
                        changes++;
                    } else {
                        newGrid2[i][j] = currentGrid[i][j];
                    }
                }
            }
            currentGrid = newGrid2;
        } while (changes > 0);

        // Count occupied seats
        int occupiedSeats = 0;
        for (Character[] characters : currentGrid) {
            for (int j = 0; j < currentGrid[0].length; j++) {
                if (characters[j] == OCCUPIED_SEAT) {
                    occupiedSeats++;
                }
            }
        }
        return occupiedSeats;
    }

    private int getNumOccupiedSeatsAdjacentTo(final Character[][] grid,
                                              final Point coordinate,
                                              final boolean part2) {
        int adjacentOccupiedSeats = 0;

        if (isSeatOccupied(grid, new Point(coordinate.x, coordinate.y), new Point(-1, -1), part2)) { // top left
            adjacentOccupiedSeats++;
        }

        if (isSeatOccupied(grid, new Point(coordinate.x, coordinate.y), new Point(-1, 0), part2)) { // top
            adjacentOccupiedSeats++;
        }

        if (isSeatOccupied(grid, new Point(coordinate.x, coordinate.y), new Point(-1, 1), part2)) { // top right
            adjacentOccupiedSeats++;
        }

        if (isSeatOccupied(grid, new Point(coordinate.x, coordinate.y), new Point(0, 1), part2)) { // right
            adjacentOccupiedSeats++;
        }

        if (isSeatOccupied(grid, new Point(coordinate.x, coordinate.y), new Point(1, 1), part2)) { // bottom right
            adjacentOccupiedSeats++;
        }

        if (isSeatOccupied(grid, new Point(coordinate.x, coordinate.y), new Point(1, 0), part2)) { // bottom
            adjacentOccupiedSeats++;
        }

        if (isSeatOccupied(grid, new Point(coordinate.x, coordinate.y), new Point(1, -1), part2)) { // bottom left
            adjacentOccupiedSeats++;
        }

        if (isSeatOccupied(grid, new Point(coordinate.x, coordinate.y), new Point(0, -1), part2)) { // left
            adjacentOccupiedSeats++;
        }

        return adjacentOccupiedSeats;
    }

    private boolean isSeatOccupied(final Character[][] grid,
                                   final Point coordinate,
                                   final Point translation,
                                   final boolean part2) {
        coordinate.translate(translation.x, translation.y);
        if (!part2) {
            return isPointInBounds(grid, coordinate) && grid[coordinate.x][coordinate.y] == OCCUPIED_SEAT;
        }
        while (isPointInBounds(grid, coordinate)) {
            if (grid[coordinate.x][coordinate.y] == OCCUPIED_SEAT) {
                return true;
            } else if (grid[coordinate.x][coordinate.y] == EMPTY_SEAT) {
                return false;
            }
            coordinate.translate(translation.x, translation.y);
        }
        return false;
    }

    private boolean isPointInBounds(final Character[][] grid,
                                    final Point coordinate) {
        return coordinate.x >= 0 && coordinate.x < grid.length && coordinate.y >= 0 && coordinate.y < grid[0].length;
    }
}
