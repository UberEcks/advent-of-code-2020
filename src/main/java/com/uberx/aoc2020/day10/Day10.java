package com.uberx.aoc2020.day10;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Collectors;

public class Day10 {
    public static List<Integer> getAdapterJoltages(final String filename) throws IOException {
        return IOUtils
                .readLines(
                        Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                        StandardCharsets.UTF_8)
                .stream()
                .map(Integer::parseInt)
                .collect(Collectors.toList());
    }

    public Pair<Integer, Integer> differencesOf1And3Jolts(final List<Integer> adapterJoltages) {
        final List<Integer> sortedAdapterJoltages = getSortedAdapterJoltages(adapterJoltages);
        int oneJoltDifferences = 0;
        int threeJoltDifference = 0;
        for (int i = 1; i < sortedAdapterJoltages.size(); i++) {
            if (sortedAdapterJoltages.get(i) - sortedAdapterJoltages.get(i - 1) == 1) {
                oneJoltDifferences++;
            } else if (sortedAdapterJoltages.get(i) - sortedAdapterJoltages.get(i - 1) == 3) {
                threeJoltDifference++;
            } else {
                throw new IllegalStateException(
                        "Unexpected difference found: "
                                + (sortedAdapterJoltages.get(i) - sortedAdapterJoltages.get(i - 1)));
            }
        }
        return Pair.of(oneJoltDifferences, threeJoltDifference);
    }

    public long adapterArrangements(final List<Integer> adapterJoltages) {
        final List<Integer> sortedAdapterJoltages = getSortedAdapterJoltages(adapterJoltages);
        final Set<Integer> requiredAdapters = new LinkedHashSet<>();
        for (int i = 1; i < sortedAdapterJoltages.size(); i++) {
            if (sortedAdapterJoltages.get(i) - sortedAdapterJoltages.get(i - 1) == 3) {
                requiredAdapters.add(sortedAdapterJoltages.get(i - 1));
                requiredAdapters.add(sortedAdapterJoltages.get(i));
            }
        }
        final Set<Integer> optionalAdapters = sortedAdapterJoltages.stream()
                .filter(adapter -> !requiredAdapters.contains(adapter))
                .collect(Collectors.toCollection(LinkedHashSet::new));
        optionalAdapters.remove(0);

        long arrangements = 1;
        int currentRequiredAdapter = sortedAdapterJoltages.get(0);
        for (int i = 1; i < sortedAdapterJoltages.size(); i++) {
            if (requiredAdapters.contains(sortedAdapterJoltages.get(i))) {
                currentRequiredAdapter = sortedAdapterJoltages.get(i);
                continue;
            }
            final List<Integer> currentAdaptersFragment = new ArrayList<>();
            currentAdaptersFragment.add(currentRequiredAdapter);
            while (sortedAdapterJoltages.get(i) < sortedAdapterJoltages.get(i - 1) + 3
                    && !requiredAdapters.contains(sortedAdapterJoltages.get(i))) {
                currentAdaptersFragment.add(sortedAdapterJoltages.get(i));
                i++;
            }
            currentAdaptersFragment.add(sortedAdapterJoltages.get(i));
            arrangements *= exhaustiveAdapterArrangements(currentAdaptersFragment);
        }
        return arrangements;
    }

    private int exhaustiveAdapterArrangements(final List<Integer> sortedAdapterJoltages) {
        final int start = sortedAdapterJoltages.get(0);
        final int end = sortedAdapterJoltages.get(sortedAdapterJoltages.size() - 1);

        final Set<Integer> adapterJoltagesLookup = new HashSet<>(sortedAdapterJoltages);
        int arrangements = 0;
        final Queue<Integer> bfs = new LinkedList<>();
        bfs.add(start);
        while (!bfs.isEmpty()) {
            final int current = bfs.poll();
            if (current == end) {
                arrangements++;
            }

            for (int i = 1; i < 4; i++) {
                if (adapterJoltagesLookup.contains(current + i)) {
                    bfs.add(current + i);
                }
            }
        }
        return arrangements;
    }

    private List<Integer> getSortedAdapterJoltages(final List<Integer> adapterJoltages) {
        adapterJoltages.add(0);
        final List<Integer> sortedAdapterJoltages = adapterJoltages
                .stream()
                .sorted()
                .collect(Collectors.toList());
        sortedAdapterJoltages.add(sortedAdapterJoltages.get(sortedAdapterJoltages.size() - 1) + 3);
        return sortedAdapterJoltages;
    }
}
