package com.uberx.aoc2020.day24;

import com.google.common.collect.ImmutableMap;
import lombok.Getter;
import lombok.ToString;
import org.apache.commons.io.IOUtils;

import java.awt.*;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Day24 {

    public static List<List<Direction>> getTileDirections(final String filename) throws IOException {
        return IOUtils
                .readLines(
                        Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                        StandardCharsets.UTF_8)
                .stream()
                .map(line -> {
                    final List<Direction> directions = new ArrayList<>();
                    int index = 0;
                    while (index < line.length()) {
                        if (line.charAt(index) == 'e' || line.charAt(index) == 'w') {
                            directions.add(Direction.valueOf(line.substring(index, index + 1).toUpperCase()));
                            index++;
                        } else {
                            directions.add(Direction.valueOf(line.substring(index, index + 2).toUpperCase()));
                            index += 2;
                        }
                    }
                    return directions;
                })
                .collect(Collectors.toList());
    }

    public long numBlackTiles1(final List<List<Direction>> tileDirections) {
        final Floor floor = createFloor(tileDirections, 0);
        flipTiles(floor, tileDirections);
        return countBlackTiles(floor.getTiles());
    }

    public long numBlackTiles2(final List<List<Direction>> tileDirections,
                               final int days) {
        final Floor floor = createFloor(tileDirections, days);
        flipTiles(floor, tileDirections);

        Floor currentFloor = new Floor(floor.getTiles());
        for (int day = 1; day <= days; day++) {
            final List<Tile> newTiles = new ArrayList<>();
            for (final Tile tile : currentFloor.getTiles()) {
                final List<Tile> neighbors = currentFloor.getNeighbors(tile);
                final Point coordinates = tile.getCoordinates();
                final long numBlackTileNeighbors = neighbors.stream()
                        .filter(neighbor -> neighbor.getColor() == Tile.Color.BLACK)
                        .count();
                if (tile.getColor() == Tile.Color.BLACK
                        && (numBlackTileNeighbors == 0 || numBlackTileNeighbors > 2)) {
                    newTiles.add(new Tile(new Point(coordinates)));
                } else if (tile.getColor() == Tile.Color.WHITE && numBlackTileNeighbors == 2) {
                    newTiles.add(new Tile(new Point(coordinates), Tile.Color.BLACK));
                } else {
                    newTiles.add(new Tile(new Point(coordinates), tile.getColor()));
                }
            }
            currentFloor = new Floor(newTiles);
        }
        return countBlackTiles(currentFloor.getTiles());
    }

    private long countBlackTiles(final List<Tile> tiles) {
        return tiles.stream()
                .filter(tile -> tile.getColor() == Tile.Color.BLACK)
                .count();
    }

    private Floor createFloor(final List<List<Direction>> tileDirections,
                              final int buffer) {
        final int maxDepth = tileDirections.stream()
                .map(List::size)
                .mapToInt(i -> i)
                .max()
                .orElse(1) + buffer;
        final int hexRows = maxDepth * 2 - 1;
        final int[] hexColumnCounts = getHexColumnCounts(hexRows);

        final List<Tile> tiles = new ArrayList<>();
        int y = hexRows / 2;
        for (final int columns : hexColumnCounts) {
            int x = -columns / 2;
            for (int i = 0; i < columns; i++) {
                tiles.add(new Tile(new Point(x, y)));
                x++;
                if (columns % 2 == 0 && x == 0) {
                    x++;
                }
            }
            y--;
        }
        return new Floor(tiles);
    }

    private int[] getHexColumnCounts(final int hexRows) {
        int[] columnCounts = new int[hexRows];

        for (int i = 0; i < hexRows; i++) {
            columnCounts[i] = hexRows - Math.abs(i - hexRows / 2);
        }

        return columnCounts;
    }

    private void flipTiles(final Floor floor,
                           final List<List<Direction>> tileDirections) {
        final Tile centerTile = floor.getTile(new Point(0, 0));
        for (final List<Direction> directions : tileDirections) {
            Tile currentTile = centerTile;
            for (final Direction direction : directions) {
                currentTile = floor.getNeighbor(currentTile, direction);
            }
            currentTile.flipColor();
        }
    }

    static class Floor {
        private static final Map<Direction, Point> DIRECTION_OFFSETS = ImmutableMap.<Direction, Point>builder()
                .put(Direction.E, new Point(1, 0))
                .put(Direction.SE, new Point(1, -1))
                .put(Direction.SW, new Point(-1, -1))
                .put(Direction.W, new Point(-1, 0))
                .put(Direction.NW, new Point(-1, 1))
                .put(Direction.NE, new Point(1, 1))
                .build();

        @Getter
        List<Tile> tiles;

        Map<Point, Tile> tileLookup;

        Floor(final List<Tile> tiles) {
            this.tiles = tiles;
            this.tileLookup = tiles.stream()
                    .collect(Collectors.toMap(
                            Tile::getCoordinates,
                            Function.identity(),
                            (v1, v2) -> v1,
                            LinkedHashMap::new));
        }

        Tile getTile(final Point coordinates) {
            return tileLookup.get(coordinates);
        }

        Tile getNeighbor(final Tile tile,
                         final Direction direction) {
            final Point tileCoordinates = tile.getCoordinates();
            final Point offset = DIRECTION_OFFSETS.get(direction);
            final Point neighborCoordinates = new Point(
                    tileCoordinates.x + offset.x,
                    tileCoordinates.y + offset.y);
            // Undo x offset for certain conditions
            if ((tileCoordinates.x < 0 && tileCoordinates.y % 2 == 0)
                    || (tileCoordinates.x > 0 && tileCoordinates.y % 2 != 0)) {
                if (direction == Direction.NE || direction == Direction.SE) {
                    neighborCoordinates.x = tileCoordinates.x;
                }
            } else if (tileCoordinates.x != 0) {
                if (direction == Direction.NW || direction == Direction.SW) {
                    neighborCoordinates.x = tileCoordinates.x;
                }
            }
            if (tileCoordinates.y % 2 != 0) {
                if (tileCoordinates.x == -1 && direction == Direction.E) {
                    neighborCoordinates.x = 1;
                } else if (tileCoordinates.x == 1 && direction == Direction.W) {
                    neighborCoordinates.x = -1;
                }
            }
            return tileLookup.get(neighborCoordinates);
        }

        List<Tile> getNeighbors(final Tile tile) {
            return Arrays.stream(Direction.values())
                    .map(direction -> getNeighbor(tile, direction))
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
        }
    }

    @ToString
    @Getter
    static class Tile {
        Point coordinates;
        Color color;

        Tile(final Point coordinates) {
            this(coordinates, Color.WHITE);
        }

        Tile(final Point coordinates,
             final Color color) {
            this.coordinates = coordinates;
            this.color = color;
        }

        void flipColor() {
            if (color == Color.WHITE) {
                color = Color.BLACK;
            } else {
                color = Color.WHITE;
            }
        }

        enum Color {
            WHITE, BLACK
        }
    }

    enum Direction {
        E, SE, SW, W, NW, NE
    }
}
