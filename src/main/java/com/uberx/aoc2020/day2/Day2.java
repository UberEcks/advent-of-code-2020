package com.uberx.aoc2020.day2;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.immutables.value.Value;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Day2 {
    public static List<Pair<PasswordPolicy, String>> getPasswordEntries(final String filename) throws IOException {
        return IOUtils
                .readLines(
                        Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                        StandardCharsets.UTF_8)
                .stream()
                .map(passwordEntry -> {
                    final String[] colonDelimitedValues = passwordEntry.split(":");
                    final String passwordPolicy = colonDelimitedValues[0];
                    final String password = colonDelimitedValues[1].trim();
                    final String[] spaceDelimitedValues = passwordPolicy.split(" ");
                    final String[] hyphenDelimitedValues = spaceDelimitedValues[0].split("-");

                    final char character = spaceDelimitedValues[1].charAt(0);
                    final int minLength = Integer.parseInt(hyphenDelimitedValues[0]);
                    final int maxLength = Integer.parseInt(hyphenDelimitedValues[1]);
                    return Pair.<PasswordPolicy, String>of(
                            ImmutablePasswordPolicy.builder()
                                    .character(character)
                                    .start(minLength)
                                    .end(maxLength)
                                    .build(),
                            password);
                })
                .collect(Collectors.toList());
    }

    public long validPasswordsPart1(final List<Pair<PasswordPolicy, String>> passwordEntries) {
        return passwordEntries
                .stream()
                .filter(entry -> entry.getKey().test1(entry.getRight()))
                .count();
    }

    public long validPasswordsPart2(final List<Pair<PasswordPolicy, String>> passwordEntries) {
        return passwordEntries
                .stream()
                .filter(entry -> entry.getKey().test2(entry.getRight()))
                .count();
    }

    @Value.Immutable
    interface PasswordPolicy {
        char getCharacter();

        int getStart();

        int getEnd();

        default boolean test1(final String password) {
            int characterCount = 0;
            for (int i = 0; i < password.length(); i++) {
                if (password.charAt(i) == getCharacter()) {
                    characterCount++;
                }
                if (characterCount > getEnd()) {
                    return false;
                }
            }
            return characterCount >= getStart();
        }

        default boolean test2(final String password) {
            return password.charAt(getStart() - 1) == getCharacter() ^ password.charAt(getEnd() - 1) == getCharacter();
        }
    }
}
