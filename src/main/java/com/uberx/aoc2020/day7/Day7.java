package com.uberx.aoc2020.day7;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Collectors;

public class Day7 {
    public static Map<String, BagNode> getBagNodes(final String filename) throws IOException {
        final List<String> input = IOUtils
                .readLines(
                        Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                        StandardCharsets.UTF_8);
        final Map<String, BagNode> bagNodes = new HashMap<>();
        input.forEach(line -> {
            final String[] links = line.split(" bags contain ");
            final String outerBag = links[0];
            final Map<String, Integer> innerBagsWithCounts = Arrays.stream(links[1].split(","))
                    .map(String::trim)
                    .map(innerBagEntry -> {
                        if (innerBagEntry.endsWith(" bag")) {
                            return innerBagEntry.substring(0, innerBagEntry.length() - 4);
                        } else if (innerBagEntry.endsWith(" bag.") || innerBagEntry.endsWith(" bags")) {
                            return innerBagEntry.substring(0, innerBagEntry.length() - 5);
                        } else if (innerBagEntry.endsWith(" bags.")) {
                            return innerBagEntry.substring(0, innerBagEntry.length() - 6);
                        }
                        throw new RuntimeException("Unexpected input: " + innerBagEntry);
                    })
                    .filter(innerBagEntry -> !"no other".equals(innerBagEntry))
                    .collect(Collectors.toMap(
                            innerBagEntry -> innerBagEntry.substring(innerBagEntry.indexOf(' ') + 1),
                            innerBagEntry -> Integer.parseInt(innerBagEntry.substring(0, innerBagEntry.indexOf(' ')))));

            // Register the bag nodes
            bagNodes.putIfAbsent(outerBag, new BagNode(outerBag));
            innerBagsWithCounts.keySet()
                    .forEach(innerBag -> bagNodes.putIfAbsent(innerBag, new BagNode(innerBag)));

            // Update bag links
            innerBagsWithCounts.forEach((innerBag, innerBagCount) -> bagNodes.get(outerBag)
                    .addContains(bagNodes.get(innerBag), innerBagCount));
        });
        return bagNodes;
    }

    public int bagsThatContain(final BagNode bagNode) {
        final Set<String> bags = new HashSet<>();
        final Queue<BagNode> bfs = new LinkedList<>();
        bfs.add(bagNode);
        while (!bfs.isEmpty()) {
            final Set<BagNode> containedBy = bfs.poll().getContainedBy();
            bfs.addAll(containedBy);
            bags.addAll(containedBy.stream()
                    .map(BagNode::getName)
                    .collect(Collectors.toList()));
        }
        return bags.size();
    }

    public int totalBagsContained(final BagNode bagNode) {
        int count = 0;
        final Queue<Pair<BagNode, Integer>> bfs = new LinkedList<>();
        bfs.add(Pair.of(bagNode, 1));
        while (!bfs.isEmpty()) {
            final Pair<BagNode, Integer> currentBagNode = bfs.poll();
            final Map<BagNode, Integer> contains = currentBagNode.getKey().getContains();
            bfs.addAll(contains.entrySet()
                    .stream()
                    .map(innerBagEntry -> Pair.of(
                            innerBagEntry.getKey(),
                            currentBagNode.getValue() * innerBagEntry.getValue()))
                    .collect(Collectors.toList()));
            count += contains.values()
                    .stream()
                    .map(innerBagCount -> innerBagCount * currentBagNode.getValue())
                    .mapToInt(i -> i)
                    .sum();
        }
        return count;
    }

    @Getter
    @ToString(of = {"name"})
    @AllArgsConstructor
    @EqualsAndHashCode(of = {"name"})
    static class BagNode {
        private final String name;
        private final Map<BagNode, Integer> contains = new HashMap<>();
        private final Set<BagNode> containedBy = new LinkedHashSet<>();

        public void addContains(final BagNode bagNode, final int count) {
            this.contains.put(bagNode, count);
            bagNode.addContainedBy(this);
        }

        private void addContainedBy(final BagNode bagNode) {
            this.containedBy.add(bagNode);
        }
    }
}
