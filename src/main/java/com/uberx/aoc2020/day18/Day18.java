package com.uberx.aoc2020.day18;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Stack;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Day18 {
    public static List<List<Character>> getExpressions(final String filename) throws IOException {
        return IOUtils
                .readLines(
                        Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                        StandardCharsets.UTF_8)
                .stream()
                .map(expression -> Arrays.stream(expression.split(""))
                        .filter(token -> !token.equals(" "))
                        .map(token -> token.charAt(0))
                        .collect(Collectors.toList()))
                .collect(Collectors.toList());
    }

    public long sumOfExpressions1(final List<List<Character>> expressions) {
        return expressions.stream()
                .map(expression -> evaluate(
                        expression,
                        args -> hasSameOrGreaterPrecedence1(args.getLeft(), args.getRight())))
                .mapToLong(i -> i)
                .sum();
    }

    public long sumOfExpressions2(final List<List<Character>> expressions) {
        return expressions.stream()
                .map(expression -> evaluate(
                        expression,
                        args -> hasSameOrGreaterPrecedence2(args.getLeft(), args.getRight())))
                .mapToLong(i -> i)
                .sum();
    }

    private long evaluate(final List<Character> tokens,
                          final Function<Pair<Character, Character>, Boolean> precedenceFunction) {
        /*
            Shunting Yard Algorithm

            1. While there are still tokens to be read in,
               1.1 Get the next token.
               1.2 If the token is:
                   1.2.1 A number: push it onto the value stack.
                   1.2.2 A variable: get its value, and push onto the value stack.
                   1.2.3 A left parenthesis: push it onto the operator stack.
                   1.2.4 A right parenthesis:
                     1 While the thing on top of the operator stack is not a
                       left parenthesis,
                         1 Pop the operator from the operator stack.
                         2 Pop the value stack twice, getting two operands.
                         3 Apply the operator to the operands, in the correct order.
                         4 Push the result onto the value stack.
                     2 Pop the left parenthesis from the operator stack, and discard it.
                   1.2.5 An operator (call it thisOp):
                     1 While the operator stack is not empty, and the top thing on the
                       operator stack has the same or greater precedence as thisOp,
                       1 Pop the operator from the operator stack.
                       2 Pop the value stack twice, getting two operands.
                       3 Apply the operator to the operands, in the correct order.
                       4 Push the result onto the value stack.
                     2 Push thisOp onto the operator stack.
            2. While the operator stack is not empty,
                1 Pop the operator from the operator stack.
                2 Pop the value stack twice, getting two operands.
                3 Apply the operator to the operands, in the correct order.
                4 Push the result onto the value stack.
            3. At this point the operator stack should be empty, and the value
               stack should have only one value in it, which is the final result.
         */
        final Stack<Long> valueStack = new Stack<>();
        final Stack<Character> operatorStack = new Stack<>();
        for (final Character token : tokens) {
            if (token >= '1' && token <= '9') {
                valueStack.push((long) Character.getNumericValue(token));
            } else if (token == '(') {
                operatorStack.push(token);
            } else if (token == ')') {
                while (operatorStack.peek() != '(') {
                    final char operator = operatorStack.pop();
                    final long operand1 = valueStack.pop();
                    final long operand2 = valueStack.pop();
                    valueStack.push(operate(operator, operand1, operand2));
                }
                operatorStack.pop(); // pop the left parenthesis
            } else { // operator
                while (!operatorStack.isEmpty() && precedenceFunction.apply(Pair.of(operatorStack.peek(), token))) {
                    final char operator = operatorStack.pop();
                    final long operand1 = valueStack.pop();
                    final long operand2 = valueStack.pop();
                    valueStack.push(operate(operator, operand1, operand2));
                }
                operatorStack.push(token);
            }
        }
        while (!operatorStack.isEmpty()) {
            final char operator = operatorStack.pop();
            final long operand1 = valueStack.pop();
            final long operand2 = valueStack.pop();
            valueStack.push(operate(operator, operand1, operand2));
        }
        return valueStack.pop();
    }

    private boolean hasSameOrGreaterPrecedence1(final char operator1,
                                                final char operator2) {
        return operator1 != '(';
    }

    private boolean hasSameOrGreaterPrecedence2(final char operator1,
                                                final char operator2) {
        if (operator1 == '+') {
            return true;
        }
        if (operator2 == '+') {
            return false;
        }
        return operator1 == '*';
    }

    private long operate(final char operator,
                         final long operand1,
                         final long operand2) {
        if (operator == '+') {
            return operand1 + operand2;
        }
        return operand1 * operand2;
    }
}
