package com.uberx.aoc2020.day4;

import org.apache.commons.io.IOUtils;
import org.immutables.value.Value;

import javax.annotation.Nullable;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;

public class Day4 {
    public static List<Passport> getPassportData(final String filename) throws IOException {
        final List<String> input = IOUtils
                .readLines(
                        Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                        StandardCharsets.UTF_8);
        final List<Passport> passportData = new ArrayList<>();
        for (int i = 0; i < input.size(); i++) {
            final Map<String, String> passportAttributes = new HashMap<>();
            while (i < input.size() && !input.get(i).isEmpty()) {
                final String[] attributes = input.get(i).split(" ");
                for (String attribute : attributes) {
                    final String[] colonDelimitedValues = attribute.split(":");
                    passportAttributes.put(colonDelimitedValues[0], colonDelimitedValues[1]);
                }
                i++;
            }
            passportData.add(Passport.fromAttributes(passportAttributes));
        }
        return passportData;
    }

    public long validPassports1(final List<Passport> passportData) {
        return passportData.stream()
                .filter(Passport::isValid1)
                .count();
    }

    public long validPassports2(final List<Passport> passportData) {
        return passportData.stream()
                .filter(Passport::isValid2)
                .count();
    }

    @Value.Immutable
    interface Passport {
        @Nullable
        Integer birthYear();

        @Nullable
        Integer issueYear();

        @Nullable
        Integer expirationYear();

        @Nullable
        String height();

        @Nullable
        String hairColor();

        @Nullable
        String eyeColor();

        @Nullable
        String passportId();

        @Nullable
        Long countryId();

        default boolean isValid1() {
            return birthYear() != null && issueYear() != null && expirationYear() != null && height() != null
                    && hairColor() != null && eyeColor() != null && passportId() != null;
        }

        default boolean isValid2() {
            final Integer birthYear = birthYear();
            final Integer issueYear = issueYear();
            final Integer expirationYear = expirationYear();
            final String height = height();
            final String hairColor = hairColor();
            final String eyeColor = eyeColor();
            final String passportId = passportId();
            return birthYear != null && birthYear >= 1920 && birthYear <= 2002
                    && issueYear != null && issueYear >= 2010 && issueYear <= 2020
                    && expirationYear != null && expirationYear >= 2020 && expirationYear <= 2030
                    && height != null &&
                    ((Pattern.matches("\\d{3}cm", height)
                            && Integer.parseInt(height.substring(0, 3)) >= 150
                            && Integer.parseInt(height.substring(0, 3)) <= 193)
                            || (Pattern.matches("\\d{2}in", height)
                            && Integer.parseInt(height.substring(0, 2)) >= 59
                            && Integer.parseInt(height.substring(0, 2)) <= 76))
                    && hairColor != null && Pattern.matches("^#[0-9a-f]{6}$", hairColor)
                    && eyeColor != null && Pattern.matches("amb|blu|brn|gry|grn|hzl|oth", eyeColor)
                    && passportId != null && Pattern.matches("^[0-9]{9}$", passportId);
        }

        static Passport fromAttributes(final Map<String, String> attributes) {
            return ImmutablePassport.builder()
                    .birthYear(attributes.get("byr") == null ? null : Integer.parseInt(attributes.get("byr")))
                    .issueYear(attributes.get("iyr") == null ? null : Integer.parseInt(attributes.get("iyr")))
                    .expirationYear(attributes.get("eyr") == null ? null : Integer.parseInt(attributes.get("eyr")))
                    .height(attributes.get("hgt"))
                    .hairColor(attributes.get("hcl"))
                    .eyeColor(attributes.get("ecl"))
                    .passportId(attributes.get("pid"))
                    .countryId(attributes.get("cid") == null ? null : Long.parseLong(attributes.get("cid")))
                    .build();
        }
    }
}
