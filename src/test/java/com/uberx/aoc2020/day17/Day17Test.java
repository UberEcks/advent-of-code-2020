package com.uberx.aoc2020.day17;

import org.junit.Test;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class Day17Test {
    @Test
    public void testDay17Part1() throws IOException {
        final Map<List<Integer>, Character> state = Day17.getInitialState("day17", 3);

        final Day17 solver = new Day17();
        assertEquals(247, solver.numActive1(state, 6));
    }

    @Test
    public void testDay17Part2() throws IOException {
        final Map<List<Integer>, Character> state = Day17.getInitialState("day17", 4);

        final Day17 solver = new Day17();
        assertEquals(1392, solver.numActive2(state, 6));
    }
}
