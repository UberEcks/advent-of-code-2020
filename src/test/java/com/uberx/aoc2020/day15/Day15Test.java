package com.uberx.aoc2020.day15;

import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class Day15Test {
    @Test
    public void testDay15Part1() throws IOException {
        final List<Integer> numbers = Day15.getNumbers("day15");

        final Day15 solver = new Day15();
        assertEquals(700, solver.spokenNumber(numbers, 2020));
    }

    @Test
    public void testDay15Part2() throws IOException {
        final List<Integer> numbers = Day15.getNumbers("day15");

        final Day15 solver = new Day15();
        assertEquals(51358, solver.spokenNumber(numbers, 30000000));
    }
}
