package com.uberx.aoc2020.day18;

import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class Day18Test {
    @Test
    public void testDay18Part1() throws IOException {
        final List<List<Character>> expressions = Day18.getExpressions("day18");

        final Day18 solver = new Day18();
        assertEquals(3159145843816L, solver.sumOfExpressions1(expressions));
    }

    @Test
    public void testDay18Part2() throws IOException {
        final List<List<Character>> expressions = Day18.getExpressions("day18");

        final Day18 solver = new Day18();
        assertEquals(55699621957369L, solver.sumOfExpressions2(expressions));
    }
}
