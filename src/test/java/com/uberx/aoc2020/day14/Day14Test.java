package com.uberx.aoc2020.day14;

import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class Day14Test {
    @Test
    public void testDay14Part1() throws IOException {
        final List<Day14.Instruction> instructions = Day14.getInstructions("day14");

        final Day14 solver = new Day14();
        assertEquals(3059488894985L, solver.sumOfMemoryValues1(instructions));
    }

    @Test
    public void testDay14Part2() throws IOException {
        final List<Day14.Instruction> instructions = Day14.getInstructions("day14");

        final Day14 solver = new Day14();
        assertEquals(2900994392308L, solver.sumOfMemoryValues2(instructions));
    }
}
