package com.uberx.aoc2020.day19;

import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class Day19Test {
    @Test
    public void testDay19Part1() throws IOException {
        final Day19.Input input = Day19.getInput("day19");

        final Day19 solver = new Day19();
        assertEquals(132, solver.messagesThatMatchRule1(
                input.getMessages(),
                input.getRules(),
                input.getRules().get("0")));
    }

    @Test
    public void testDay19Part2() throws IOException {
        final Day19.Input input = Day19.getInput("day19");

        final Day19 solver = new Day19();
        assertEquals(306, solver.messagesThatMatchRule2(
                input.getMessages(),
                input.getRules(),
                input.getRules().get("0")));
    }
}
