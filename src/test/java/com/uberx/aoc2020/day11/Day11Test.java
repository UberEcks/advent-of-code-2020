package com.uberx.aoc2020.day11;

import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class Day11Test {
    @Test
    public void testDay11Part1() throws IOException {
        final Character[][] grid = Day11.getSeatGrid("day11");

        final Day11 solver = new Day11();
        assertEquals(2468, solver.numSeatsOccupied(grid, 4, false));
    }

    @Test
    public void testDay11Part2() throws IOException {
        final Character[][] grid = Day11.getSeatGrid("day11");

        final Day11 solver = new Day11();
        assertEquals(2214, solver.numSeatsOccupied(grid, 5, true));
    }
}
