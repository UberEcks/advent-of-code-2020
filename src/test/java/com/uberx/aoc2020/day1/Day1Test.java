package com.uberx.aoc2020.day1;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class Day1Test {

    @Test
    public void testDay1Part1() throws IOException {
        final Long[] entries = Day1.getExpenseEntries("day1");

        final Day1 solver = new Day1();
        final Pair<Long, Long> pairWithSum2020 = solver.pairWithSum(entries, 2020);
        assertEquals(731731, pairWithSum2020.getLeft() * pairWithSum2020.getRight());
    }

    @Test
    public void testDay1Part2() throws IOException {
        final Long[] entries = Day1.getExpenseEntries("day1");

        final Day1 solver = new Day1();
        final List<Long> tripletWithSum2020 = solver.tripletWithSum(entries, 2020);
        assertEquals(
                116115990,
                tripletWithSum2020.get(0) * tripletWithSum2020.get(1) * tripletWithSum2020.get(2));
    }
}
