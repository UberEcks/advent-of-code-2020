package com.uberx.aoc2020.day3;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class Day3Test {

    @Test
    public void testDay3Part1() throws IOException {
        final Character[][] grid = Day3.getGrid("day3");

        final Day3 solver = new Day3();
        assertEquals(156, solver.treeEncounters(grid, ImmutablePair.of(1, 3)));
    }

    @Test
    public void testDay3Part2() throws IOException {
        final Character[][] grid = Day3.getGrid("day3");

        final Day3 solver = new Day3();
        final List<ImmutablePair<Integer, Integer>> slopes = Lists.newArrayList(
                ImmutablePair.of(1, 1),
                ImmutablePair.of(1, 3),
                ImmutablePair.of(1, 5),
                ImmutablePair.of(1, 7),
                ImmutablePair.of(2, 1));
        assertEquals(
                new Long(3521829480L),
                slopes.stream()
                        .map(slope -> solver.treeEncounters(grid, slope))
                        .reduce(1L, (a, b) -> a * b));
    }
}
