package com.uberx.aoc2020.day23;

import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class Day23Test {
    @Test
    public void testDay23Part1() throws IOException {
        final Day23.CyclicalDoublyLinkedList input = Day23.getInput("day23");

        final Day23 solver = new Day23();
        assertEquals("54327968", solver.labelsAfterCup1(input, 100));
    }

    @Test
    public void testDay23Part2() throws IOException {
        final Day23.CyclicalDoublyLinkedList input = Day23.getInput("day23", 1000000);

        final Day23 solver = new Day23();
        assertEquals(157410423276L, solver.productOf2ImmediateLabelsAfterCup1(input, 10000000));
    }
}
