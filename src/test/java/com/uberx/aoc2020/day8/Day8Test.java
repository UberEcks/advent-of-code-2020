package com.uberx.aoc2020.day8;

import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class Day8Test {
    @Test
    public void testDay8Part1() throws IOException {
        final List<Day8.Instruction> instructions = Day8.getInstructions("day8");

        final Day8 solver = new Day8();
        assertEquals(1930, solver.accumulatorBeforeLoop(instructions));
    }

    @Test
    public void testDay8Part2() throws IOException {
        final List<Day8.Instruction> instructions = Day8.getInstructions("day8");

        final Day8 solver = new Day8();
        assertEquals(1688, solver.accumulatorAfterFix(instructions));
    }
}
