package com.uberx.aoc2020.day25;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class Day25Test {
    @Test
    public void testDay25Part1() throws IOException {
        final Pair<Integer, Integer> publicKeys = Day25.getPublicKeys("day25");

        final Day25 solver = new Day25();
        assertEquals(18608573, solver.encryptionKey(publicKeys.getLeft(), publicKeys.getRight()));
        assertEquals(18608573, solver.encryptionKey(publicKeys.getRight(), publicKeys.getLeft()));
    }
}
