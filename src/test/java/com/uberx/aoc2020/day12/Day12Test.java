package com.uberx.aoc2020.day12;

import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class Day12Test {
    @Test
    public void testDay12Part1() throws IOException {
        final List<Day12.Navigation> navigationInstructions = Day12.getNavigationInstructions("day12");

        final Day12 solver = new Day12();
        assertEquals(1319, solver.manhattanDistance1(navigationInstructions));
    }

    @Test
    public void testDay12Part2() throws IOException {
        final List<Day12.Navigation> navigationInstructions = Day12.getNavigationInstructions("day12");

        final Day12 solver = new Day12();
        assertEquals(62434, solver.manhattanDistance2(navigationInstructions));
    }
}
