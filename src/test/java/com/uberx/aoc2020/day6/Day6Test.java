package com.uberx.aoc2020.day6;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class Day6Test {
    @Test
    public void testDay6Part1() throws IOException {
        final List<Pair<String, Long>> uniqueAnswerGroups = Day6.getUniqueAnswerGroups("day6");

        final Day6 solver = new Day6();
        assertEquals(6585, solver.sumOfUniqueAnswerGroupCounts(uniqueAnswerGroups));
    }

    @Test
    public void testDay6Part2() throws IOException {
        final List<Pair<String, Long>> uniqueAnswerGroups = Day6.getUniqueAnswerGroups("day6");

        final Day6 solver = new Day6();
        assertEquals(3276, solver.sumOfUniqueAnswerGroupAllCounts(uniqueAnswerGroups));
    }
}
