package com.uberx.aoc2020.day16;

import org.junit.Test;

import java.io.IOException;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class Day16Test {
    @Test
    public void testDay16Part1() throws IOException {
        final Day16.Ticket ticket = Day16.getTicket("day16");

        final Day16 solver = new Day16();
        assertEquals(20091, solver.ticketErrorRate(ticket.getRules(), ticket.getNearbyTickets()));
    }

    @Test
    public void testDay16Part2() throws IOException {
        final Day16.Ticket ticket = Day16.getTicket("day16");

        final Day16 solver = new Day16();
        final Map<String, Integer> ruleNameToIndexMapping = solver.ruleNameToIndexMapping(
                ticket.getRules(), ticket.getNearbyTickets());
        assertEquals(
                2325343130651L,
                new Long(ticket.getYourTicket().get(ruleNameToIndexMapping.get("departure location")))
                        * new Long(ticket.getYourTicket().get(ruleNameToIndexMapping.get("departure station")))
                        * new Long(ticket.getYourTicket().get(ruleNameToIndexMapping.get("departure platform")))
                        * new Long(ticket.getYourTicket().get(ruleNameToIndexMapping.get("departure track")))
                        * new Long(ticket.getYourTicket().get(ruleNameToIndexMapping.get("departure date")))
                        * new Long(ticket.getYourTicket().get(ruleNameToIndexMapping.get("departure time"))));
    }
}
