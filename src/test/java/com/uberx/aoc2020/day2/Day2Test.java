package com.uberx.aoc2020.day2;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class Day2Test {

    @Test
    public void testDay2Part1() throws IOException {
        final List<Pair<Day2.PasswordPolicy, String>> passwordEntries = Day2.getPasswordEntries("day2");

        final Day2 solver = new Day2();
        assertEquals(524, solver.validPasswordsPart1(passwordEntries));
    }

    @Test
    public void testDay2Part2() throws IOException {
        final List<Pair<Day2.PasswordPolicy, String>> passwordEntries = Day2.getPasswordEntries("day2");

        final Day2 solver = new Day2();
        assertEquals(485, solver.validPasswordsPart2(passwordEntries));
    }
}
