package com.uberx.aoc2020.day24;

import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class Day24Test {
    @Test
    public void testDay24Part1() throws IOException {
        final List<List<Day24.Direction>> tileDirections = Day24.getTileDirections("day24");

        final Day24 solver = new Day24();
        assertEquals(332, solver.numBlackTiles1(tileDirections));
    }

    @Test
    public void testDay24Part2() throws IOException {
        final List<List<Day24.Direction>> tileDirections = Day24.getTileDirections("day24");

        final Day24 solver = new Day24();
        assertEquals(3900, solver.numBlackTiles2(tileDirections, 100));
    }
}
