package com.uberx.aoc2020.day5;

import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class Day5Test {
    @Test
    public void testDay5Part1() throws IOException {
        final List<String> boardingPasses = Day5.getBoardingPasses("day5");

        final Day5 solver = new Day5();
        assertEquals(838, (int) solver.maxSeatId(solver.getSeatIds(boardingPasses)));
    }

    @Test
    public void testDay5Part2() throws IOException {
        final List<String> boardingPasses = Day5.getBoardingPasses("day5");

        final Day5 solver = new Day5();
        assertEquals(714, (int) solver.missingSeatId(solver.getSeatIds(boardingPasses)));
    }
}
