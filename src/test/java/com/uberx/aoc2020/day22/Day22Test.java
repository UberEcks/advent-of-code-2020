package com.uberx.aoc2020.day22;

import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class Day22Test {
    @Test
    public void testDay22Part1() throws IOException {
        final Day22.Combat combat = Day22.getPlayerCards("day22");

        final Day22 solver = new Day22();
        assertEquals(32598, solver.winningScore1(combat));
    }

    @Test
    public void testDay22Part2() throws IOException {
        final Day22.Combat combat = Day22.getPlayerCards("day22");

        final Day22 solver = new Day22();
        assertEquals(35836, solver.winningScore2(combat));
    }
}
