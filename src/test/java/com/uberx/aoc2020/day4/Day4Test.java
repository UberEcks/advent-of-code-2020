package com.uberx.aoc2020.day4;

import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class Day4Test {
    @Test
    public void testDay4Part1() throws IOException {
        final List<Day4.Passport> passportData = Day4.getPassportData("day4");

        final Day4 solver = new Day4();
        assertEquals(239, solver.validPassports1(passportData));
    }

    @Test
    public void testDay4Part2() throws IOException {
        final List<Day4.Passport> passportData = Day4.getPassportData("day4");

        final Day4 solver = new Day4();
        assertEquals(188, solver.validPassports2(passportData));
    }
}
