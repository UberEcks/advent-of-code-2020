package com.uberx.aoc2020.day20;

import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class Day20Test {
    @Test
    public void testDay20Part1() throws IOException {
        final List<Day20.Tile> tiles = Day20.getTiles("day20");

        final Day20 solver = new Day20();
        assertEquals(32287787075651L, solver.productOfCornerTileIds(tiles));
    }

    @Test
    public void testDay20Part2() throws IOException {
        final List<Day20.Tile> tiles = Day20.getTiles("day20");

        final Day20 solver = new Day20();
        assertEquals(1939, solver.waterRoughnessOfFinalImageWithSeaMonsters(tiles, 12, 12));
    }
}
