package com.uberx.aoc2020.day21;

import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class Day21Test {
    @Test
    public void testDay21Part1() throws IOException {
        final List<Day21.Food> foods = Day21.getFoods("day21");

        final Day21 solver = new Day21();
        assertEquals(2595, solver.numOccurrencesOfIngredientsWithoutAllergens(foods));
    }

    @Test
    public void testDay21Part2() throws IOException {
        final List<Day21.Food> foods = Day21.getFoods("day21");

        final Day21 solver = new Day21();
        assertEquals(
                "thvm,jmdg,qrsczjv,hlmvqh,zmb,mrfxh,ckqq,zrgzf",
                solver.canonicalDangerousIngredientList(foods));
    }
}
