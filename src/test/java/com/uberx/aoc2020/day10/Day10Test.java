package com.uberx.aoc2020.day10;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class Day10Test {
    @Test
    public void testDay10Part1() throws IOException {
        final List<Integer> adapterJoltages = Day10.getAdapterJoltages("day10");

        final Day10 solver = new Day10();
        final Pair<Integer, Integer> differencesOf1And3Jolts = solver.differencesOf1And3Jolts(adapterJoltages);
        assertEquals(2176, differencesOf1And3Jolts.getKey() * differencesOf1And3Jolts.getValue());
    }

    @Test
    public void testDay10Part2() throws IOException {
        final List<Integer> adapterJoltages = Day10.getAdapterJoltages("day10");

        final Day10 solver = new Day10();
        assertEquals(18512297918464L, solver.adapterArrangements(adapterJoltages));
    }
}
