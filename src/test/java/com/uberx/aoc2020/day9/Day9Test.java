package com.uberx.aoc2020.day9;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class Day9Test {
    @Test
    public void testDay9() throws IOException {
        final List<Long> numbers = Day9.getInput("day9");

        final Day9 solver = new Day9();
        final Pair<Long, Integer> invalidNumber = solver.invalidNumber(numbers, 25);
        assertEquals(new Long(1721308972), invalidNumber.getKey());
        assertEquals(209694133, solver.encryptionWeakness(numbers, invalidNumber));
    }
}
