package com.uberx.aoc2020.day7;

import org.junit.Test;

import java.io.IOException;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class Day7Test {
    @Test
    public void testDay7Part1() throws IOException {
        final Map<String, Day7.BagNode> bagNodes = Day7.getBagNodes("day7");

        final Day7 solver = new Day7();
        assertEquals(121, solver.bagsThatContain(bagNodes.get("shiny gold")));
    }

    @Test
    public void testDay7Part2() throws IOException {
        final Map<String, Day7.BagNode> bagNodes = Day7.getBagNodes("day7");

        final Day7 solver = new Day7();
        assertEquals(3805, solver.totalBagsContained(bagNodes.get("shiny gold")));
    }
}
