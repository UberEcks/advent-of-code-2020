package com.uberx.aoc2020.day13;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class Day13Test {
    @Test
    public void testDay13Part1() throws IOException {
        final Pair<Integer, List<Integer>> input = Day13.getInput("day13");

        final Day13 solver = new Day13();
        final Pair<Integer, Integer> earliestTimestampAndBusId = solver.earliestTimestampAndBusId(input);
        assertEquals(
                410,
                (earliestTimestampAndBusId.getKey() - input.getLeft()) * earliestTimestampAndBusId.getValue());
    }

    @Test
    public void testDay13Part2() throws IOException {
        final Pair<Integer, List<Integer>> input = Day13.getInput("day13");

        final Day13 solver = new Day13();
        assertEquals(
                600691418730595L,
                solver.earliestTimestampWithMatchingOffsets(input.getRight(), 100000000000000L));
    }
}
